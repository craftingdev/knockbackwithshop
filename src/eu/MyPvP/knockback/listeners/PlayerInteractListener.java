package eu.MyPvP.knockback.listeners;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;

import eu.MyPvP.knockback.KnockBack;

/*
* Created by MarvCode at 24.08.2019 22:22:41
*/

public class PlayerInteractListener implements Listener {
	private KnockBack plugin;

	public PlayerInteractListener(final KnockBack plugin) {
		this.plugin = plugin;
	}

	final ArrayList<Player> players = new ArrayList<Player>();
	public int i = 0;

	@EventHandler
	public void onInteract(final PlayerInteractEvent e) {
		if (e.getItem() == null)
			return;

		if (e.getItem().getItemMeta() == null)
			return;

		if (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			if (e.getItem().getType().equals(Material.FISHING_ROD)) {
				if (players.contains(e.getPlayer())) {
					e.getPlayer().sendMessage(plugin.getPrefix() + "�7Bitte warte noch �e" + (30 - i) + " �7Sekunden!");
					e.setCancelled(true);
					return;
				} else {
					players.add(e.getPlayer());
				}
				i = 0;

				new BukkitRunnable() {
					@Override
					public void run() {
						if (e.getPlayer() == null)
							this.cancel();

						if (i >= 29) {
							if (players.contains(e.getPlayer())) {
								players.remove(e.getPlayer());
								e.getPlayer().sendMessage(
										plugin.getPrefix() + "�7Du kannst die �cLegend�re Angel �7nun wieder nutzen.");
							}
						} else {
							i++;
						}

					}
				}.runTaskTimer(plugin, 0, 20);
			}

		}
	}

}

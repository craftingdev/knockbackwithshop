package eu.MyPvP.knockback.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import eu.MyPvP.knockback.KnockBack;
import eu.MyPvP.knockback.utils.ItemCreator;
import eu.MyPvP.knockback.utils.PlayerInventory;
import ru.tehkode.permissions.bukkit.PermissionsEx;

/*
* Created by MarvCode at 21.08.2019 21:47:22
*/

public class PlayerDeathListener implements Listener {

	private KnockBack plugin;

	public PlayerDeathListener(final KnockBack plugin) {
		this.plugin = plugin;
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onDeath(final PlayerDeathEvent e) {

		Player p = e.getEntity();
		Player h = e.getEntity().getKiller();

		e.setDeathMessage(null);
		e.getDrops().clear();

		if (h == null) {
			plugin.getData().getPlayerDatas().get(p.getUniqueId()).addDeaths();
			plugin.getData().getPlayerDatas().get(p.getUniqueId()).addDailyDeaths();
			p.sendMessage(plugin.getPrefix() + "�7Du bist gestorben!");
			p.setLevel(0);
			plugin.getScoreboard().set(p);
			p.spigot().respawn();
			PlayerInventory.giveInventory(p);

		}

		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "kill @e[type=ThrownEnderpearl,Name=" + p.getName() + "]");
		if (h != null && p != h) {
			PlayerInventory.giveInventory(p);
			if (plugin.getData().getNemesis().containsKey(h)) {
				if (plugin.getData().getNemesis().get(h).equals(p)) {
					h.sendMessage(plugin.getPrefix() + "�7Du hast deinen �eNemesisgegner �7get�tet.");
				}
			}
			p.setLevel(0);
			plugin.getData().getPlayerDatas().get(p.getUniqueId()).addDeaths();
			plugin.getData().getPlayerDatas().get(p.getUniqueId()).addDailyDeaths();
			plugin.getScoreboard().set(p);
			plugin.getData().getPlayerDatas().get(h.getUniqueId()).addDailyKills();
			plugin.getData().getPlayerDatas().get(h.getUniqueId()).addKills();
			plugin.getData().getNemesis().put(p, h);
			p.sendMessage(plugin.getPrefix() + "�7Dein �eNemesisgegner �7ist nun �e"
					+ PermissionsEx.getPermissionManager().getUser(h.getUniqueId()).getGroups()[0].getOption("color")
							.replace("&", "�")
					+ h.getName() + "�7.");
			p.sendMessage(plugin.getPrefix() + "�7Du wurdest von �e"
					+ PermissionsEx.getPermissionManager().getUser(h.getUniqueId()).getGroups()[0].getOption("color")
							.replace("&", "�")
					+ h.getName() + " �7get�tet!");
			h.sendMessage(plugin.getPrefix() + "�7Du hast �e"
					+ PermissionsEx.getPermissionManager().getUser(p.getUniqueId()).getGroups()[0].getOption("color")
							.replace("&", "�")
					+ p.getName() + " �7get�tet.");
			h.playSound(h.getLocation(), Sound.LEVEL_UP, 10, 10);
			if (!h.hasPermission("knockback.doublecoins")) {
				plugin.getCoinsAPI().addCoins(h.getUniqueId(), 1);
				h.sendTitle("", "�eCoins �7+�e1");
			} else {
				plugin.getCoinsAPI().addCoins(h.getUniqueId(), 2);
				h.sendTitle("", "�eCoins �7+�e2");
			}

			h.giveExpLevels(1);
			plugin.getScoreboard().set(h);
			if (h.getLevel() == 3 || h.getLevel() == 5 || h.getLevel() == 7 || h.getLevel() == 10 || h.getLevel() == 15
					|| h.getLevel() == 20 || h.getLevel() == 25 || h.getLevel() == 30) {
				Bukkit.broadcastMessage(plugin.getPrefix() + "�c"
						+ PermissionsEx.getPermissionManager().getUser(h.getUniqueId()).getGroups()[0]
								.getOption("color").replace("&", "�")
						+ h.getName() + " �7hat eine �e" + h.getLevel() + "�7er Killstreak!");
				plugin.getInventorySlots().getEnderpearl(h.getUniqueId(), slot -> {
					h.getInventory().setItem(slot, new ItemCreator(Material.ENDER_PEARL).setAmount(1).build());
				});
				plugin.getCoinsAPI().addCoins(h.getUniqueId(), h.getLevel());
				h.sendMessage(plugin.getPrefix() + "�7Du hast �e" + h.getLevel() + " �7Coins f�r die �e" + h.getLevel()
						+ "�7er Killstreak erhalten.");
			}
			h.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 2 * 20, 5));
			p.spigot().respawn();

		}

	}
}
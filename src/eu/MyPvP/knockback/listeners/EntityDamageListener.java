package eu.MyPvP.knockback.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import eu.MyPvP.knockback.KnockBack;

/*
* Created by MarvCode at 23.08.2019 10:31:59
*/

public class EntityDamageListener implements Listener {
	@SuppressWarnings("unused")
	private KnockBack plugin;

	public EntityDamageListener(final KnockBack plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onDamge(final EntityDamageEvent e) {
		if(e.getCause().equals(DamageCause.FALL)) {
			e.setDamage(0);
		}
	}
}

package eu.MyPvP.knockback.utils;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import eu.MyPvP.knockback.KnockBack;
import net.minecraft.server.v1_8_R3.IScoreboardCriteria;
import net.minecraft.server.v1_8_R3.Packet;
import net.minecraft.server.v1_8_R3.PacketPlayOutScoreboardDisplayObjective;
import net.minecraft.server.v1_8_R3.PacketPlayOutScoreboardObjective;
import net.minecraft.server.v1_8_R3.PacketPlayOutScoreboardScore;
import net.minecraft.server.v1_8_R3.Scoreboard;
import net.minecraft.server.v1_8_R3.ScoreboardObjective;
import net.minecraft.server.v1_8_R3.ScoreboardScore;

/*
* Created by MarvCode at 20.08.2019 21:56:32
*/

public class KnockBackScore {
	private KnockBack plugin;

	public KnockBackScore(final KnockBack plugin) {
		this.plugin = plugin;
	}

	public void set(Player p) {

		Scoreboard scoreboard = new Scoreboard();
		ScoreboardObjective obj = scoreboard.registerObjective("test", IScoreboardCriteria.b);
		obj.setDisplayName("§f§lKNOCKBACK");
		PacketPlayOutScoreboardObjective createPacket = new PacketPlayOutScoreboardObjective(obj, 0);
		PacketPlayOutScoreboardDisplayObjective display = new PacketPlayOutScoreboardDisplayObjective(1, obj);
		int kills = -1;

		try {
			kills = plugin.getData().getPlayerDatas().get(p.getUniqueId()).getKills();
		} catch (Exception e) {
		}
		int deaths = -1;

		try {
			deaths = plugin.getData().getPlayerDatas().get(p.getUniqueId()).getDeaths();
		} catch (Exception e) {
		}

		ScoreboardScore score1 = new ScoreboardScore(scoreboard, obj, "§c                ");
		ScoreboardScore score2 = new ScoreboardScore(scoreboard, obj, "§fKills:");
		ScoreboardScore score3 = new ScoreboardScore(scoreboard, obj, "§8» §e" + (kills == -1 ? "§7lädt" : kills));
		ScoreboardScore score4 = new ScoreboardScore(scoreboard, obj, "§a ");
		ScoreboardScore score5 = new ScoreboardScore(scoreboard, obj, "§fTode:");
		ScoreboardScore score6 = new ScoreboardScore(scoreboard, obj, "§8» §c" + (deaths == -1 ? "§7lädt" : deaths));
		ScoreboardScore score7 = new ScoreboardScore(scoreboard, obj, "§c  ");
		ScoreboardScore score8 = new ScoreboardScore(scoreboard, obj, "§fKillstreak:");
		ScoreboardScore score9 = new ScoreboardScore(scoreboard, obj, "§8» §9" + p.getLevel());
		ScoreboardScore score10 = new ScoreboardScore(scoreboard, obj, "§d ");
		ScoreboardScore score11 = new ScoreboardScore(scoreboard, obj, "§fMap:");
		ScoreboardScore score12 = new ScoreboardScore(scoreboard, obj,
				"§8» §d" + plugin.getMapChange().getCurrentMap());
		ScoreboardScore score13 = new ScoreboardScore(scoreboard, obj, "§e ");

		score1.setScore(10);
		score2.setScore(9);
		score3.setScore(8);
		score4.setScore(7);
		score5.setScore(6);
		score6.setScore(5);
		score7.setScore(4);
		score8.setScore(3);
		score9.setScore(2);
		score10.setScore(1);
		score11.setScore(-1);
		score12.setScore(-2);
		score13.setScore(-3);

		PacketPlayOutScoreboardObjective removePacket = new PacketPlayOutScoreboardObjective(obj, 1);
		PacketPlayOutScoreboardScore packet1 = new PacketPlayOutScoreboardScore(score1);
		PacketPlayOutScoreboardScore packet2 = new PacketPlayOutScoreboardScore(score2);
		PacketPlayOutScoreboardScore packet3 = new PacketPlayOutScoreboardScore(score3);
		PacketPlayOutScoreboardScore packet4 = new PacketPlayOutScoreboardScore(score4);
		PacketPlayOutScoreboardScore packet5 = new PacketPlayOutScoreboardScore(score5);
		PacketPlayOutScoreboardScore packet6 = new PacketPlayOutScoreboardScore(score6);
		PacketPlayOutScoreboardScore packet7 = new PacketPlayOutScoreboardScore(score7);
		PacketPlayOutScoreboardScore packet8 = new PacketPlayOutScoreboardScore(score8);
		PacketPlayOutScoreboardScore packet9 = new PacketPlayOutScoreboardScore(score9);
		PacketPlayOutScoreboardScore packet10 = new PacketPlayOutScoreboardScore(score10);
		PacketPlayOutScoreboardScore packet11 = new PacketPlayOutScoreboardScore(score11);
		PacketPlayOutScoreboardScore packet12 = new PacketPlayOutScoreboardScore(score12);
		PacketPlayOutScoreboardScore packet13 = new PacketPlayOutScoreboardScore(score13);
		sendPacket(removePacket, p);
		sendPacket(createPacket, p);
		sendPacket(display, p);

		sendPacket(packet1, p);
		sendPacket(packet2, p);
		sendPacket(packet3, p);
		sendPacket(packet4, p);
		sendPacket(packet5, p);
		sendPacket(packet6, p);
		sendPacket(packet7, p);
		sendPacket(packet8, p);
		sendPacket(packet9, p);
		sendPacket(packet10, p);
		sendPacket(packet11, p);
		sendPacket(packet12, p);
		sendPacket(packet13, p);
	}

	@SuppressWarnings("rawtypes")
	private void sendPacket(Packet packet, Player p) {
		((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
	}
}
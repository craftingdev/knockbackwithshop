package eu.MyPvP.knockback.mysql;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;
import java.util.function.Consumer;

import eu.MyPvP.knockback.KnockBack;

/*
* Created by MarvCode at 22.08.2019 15:46:06
*/

public class InventorySlots {
	private KnockBack plugin;

	public InventorySlots(final KnockBack plugin) {
		this.plugin = plugin;
	}

	private HashMap<UUID, Integer> block = new HashMap<UUID, Integer>();
	private HashMap<UUID, Integer> stick = new HashMap<UUID, Integer>();
	private HashMap<UUID, Integer> enderpearl = new HashMap<UUID, Integer>();
	private HashMap<UUID, Integer> cobweb = new HashMap<UUID, Integer>();
	private HashMap<UUID, Integer> bow = new HashMap<UUID, Integer>();
	private HashMap<UUID, Integer> arrow = new HashMap<UUID, Integer>();

	public void isUserExisting(final UUID uuid, final Consumer<Boolean> consumer) {

		plugin.getAsyncMySQL().query("SELECT * FROM inventory_knockback WHERE UUID = '" + uuid + "'", exists -> {
			try {

				consumer.accept(exists.next());
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}

	public void getStone(final UUID uuid, final Consumer<Integer> consumer) {
		if (this.block.containsKey(uuid)) {
			if (block.get(uuid) == null) {
				consumer.accept(1);
				return;
			}

			consumer.accept(this.block.get(uuid));
			return;
		}

		plugin.getAsyncMySQL().query("SELECT Block FROM inventory_knockback WHERE UUID = '" + uuid + "'", item -> {
			try {
				if (item.next()) {
					this.block.put(uuid, item.getInt("Block"));
					consumer.accept(item.getInt("Block"));

				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}

	public void setStone(final UUID uuid, final int slot) {
		isUserExisting(uuid, exists -> {
			if (exists) {
				plugin.getAsyncMySQL()
						.update("UPDATE inventory_knockback SET Block = '" + slot + "' WHERE UUID = '" + uuid + "'");

				this.block.put(uuid, slot);
			} else {
				this.createPlayer(uuid);
			}
		});

	}

	public void getCobweb(final UUID uuid, final Consumer<Integer> consumer) {
		if (this.cobweb.containsKey(uuid)) {
			consumer.accept(this.cobweb.get(uuid));
			return;
		}

		plugin.getAsyncMySQL().query("SELECT Cobweb FROM inventory_knockback WHERE UUID = '" + uuid + "'", item -> {
			try {
				if (item.next()) {
					this.cobweb.put(uuid, item.getInt("Cobweb"));
					consumer.accept(item.getInt("Cobweb"));

				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}

	public void setCobweb(final UUID uuid, final int slot) {
		isUserExisting(uuid, exists -> {
			if (exists) {
				plugin.getAsyncMySQL()
						.update("UPDATE inventory_knockback SET Cobweb = '" + slot + "' WHERE UUID = '" + uuid + "'");

				this.cobweb.put(uuid, slot);

			} else {
				this.createPlayer(uuid);
			}
		});
	}

	public void getStick(final UUID uuid, final Consumer<Integer> consumer) {
		if (this.stick.containsKey(uuid)) {
			consumer.accept(this.stick.get(uuid));
			return;
		}

		plugin.getAsyncMySQL().query("SELECT Stick FROM inventory_knockback WHERE UUID = '" + uuid + "'", item -> {
			try {
				if (item.next()) {
					this.stick.put(uuid, item.getInt("Stick"));
					consumer.accept(item.getInt("Stick"));

				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}

	public void setStick(final UUID uuid, final int slot) {
		isUserExisting(uuid, exists -> {
			if (exists) {
				plugin.getAsyncMySQL()
						.update("UPDATE inventory_knockback SET Stick = '" + slot + "' WHERE UUID = '" + uuid + "'");

				this.stick.put(uuid, slot);

			} else {
				this.createPlayer(uuid);
			}
		});
	}

	public void getBow(final UUID uuid, final Consumer<Integer> consumer) {
		if (this.bow.containsKey(uuid)) {
			consumer.accept(this.bow.get(uuid));
			return;
		}

		plugin.getAsyncMySQL().query("SELECT Bow FROM inventory_knockback WHERE UUID = '" + uuid + "'", item -> {
			try {
				if (item.next()) {
					this.bow.put(uuid, item.getInt("Bow"));
					consumer.accept(item.getInt("Bow"));

				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}

	public void setBow(final UUID uuid, final int slot) {
		isUserExisting(uuid, exists -> {
			if (exists) {
				plugin.getAsyncMySQL()
						.update("UPDATE inventory_knockback SET Bow = '" + slot + "' WHERE UUID = '" + uuid + "'");

				this.bow.put(uuid, slot);

			} else {
				this.createPlayer(uuid);
			}
		});
	}

	public void getArrow(final UUID uuid, final Consumer<Integer> consumer) {
		if (this.arrow.containsKey(uuid)) {
			consumer.accept(this.arrow.get(uuid));
			return;
		}

		plugin.getAsyncMySQL().query("SELECT Arrow FROM inventory_knockback WHERE UUID = '" + uuid + "'", item -> {
			try {
				if (item.next()) {
					this.arrow.put(uuid, item.getInt("Arrow"));
					consumer.accept(item.getInt("Arrow"));

				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}

	public void setArrow(final UUID uuid, final int slot) {
		isUserExisting(uuid, exists -> {
			if (exists) {
				plugin.getAsyncMySQL()
						.update("UPDATE inventory_knockback SET Arrow = '" + slot + "' WHERE UUID = '" + uuid + "'");

				this.arrow.put(uuid, slot);

			} else {
				this.createPlayer(uuid);
			}
		});
	}

	public void getEnderpearl(final UUID uuid, final Consumer<Integer> consumer) {
		if (this.enderpearl.containsKey(uuid)) {
			consumer.accept(this.enderpearl.get(uuid));
			return;
		}

		plugin.getAsyncMySQL().query("SELECT Enderpearl FROM inventory_knockback WHERE UUID = '" + uuid + "'", item -> {
			try {
				if (item.next()) {
					this.enderpearl.put(uuid, item.getInt("Enderpearl"));
					consumer.accept(item.getInt("Enderpearl"));

				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}

	public void setEnderpearl(final UUID uuid, final int slot) {
		isUserExisting(uuid, exists -> {
			if (exists) {
				plugin.getAsyncMySQL().update(
						"UPDATE inventory_knockback SET Enderpearl = '" + slot + "' WHERE UUID = '" + uuid + "'");

				this.enderpearl.put(uuid, slot);

			} else {
				this.createPlayer(uuid);
			}
		});
	}

	public void createPlayer(final UUID uuid) {
		isUserExisting(uuid, exists -> {
			if (!exists) {
				plugin.getAsyncMySQL().update(
						"INSERT INTO inventory_knockback (UUID, Bow, Arrow, Cobweb, Block, Stick, Enderpearl) VALUES ('"
								+ uuid + "', '2', '6', '7', '1', '0', '8')");
			}
		});
	}
}
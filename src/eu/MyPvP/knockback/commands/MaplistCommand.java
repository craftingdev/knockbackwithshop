package eu.MyPvP.knockback.commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import eu.MyPvP.knockback.KnockBack;

/*
* Created by MarvCode at 20.08.2019 21:46:00
*/

public class MaplistCommand implements CommandExecutor {
	private KnockBack plugin;

	public MaplistCommand(final KnockBack plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender s, Command c, String l, String[] a) {
		if (!(s instanceof Player))
			return true;

		final Player p = (Player) s;

		if (plugin.getLocationManager().getLocations().isEmpty()) {
			p.sendMessage(plugin.getPrefix() + "�7Es gibt keine Maps.");
			return true;
		}

		p.sendMessage(plugin.getPrefix() + "�7Es gibt folgende Maps:");
		for (final Location map : plugin.getLocationManager().getLocations().values()) {
			p.sendMessage(" �8- �7" + plugin.getData()
					.getKey(plugin.getLocationManager().getLocations(), map));
		}

		p.sendMessage(plugin.getPrefix());
		return false;
	}

}
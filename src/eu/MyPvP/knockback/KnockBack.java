package eu.MyPvP.knockback;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import eu.MyPvP.coinsaddon.utils.CoinsAPI;
import eu.MyPvP.knockback.commands.BuildCommand;
import eu.MyPvP.knockback.commands.CoinsCommand;
import eu.MyPvP.knockback.commands.InvCommand;
import eu.MyPvP.knockback.commands.KnockBackCommand;
import eu.MyPvP.knockback.commands.MaplistCommand;
import eu.MyPvP.knockback.commands.ReloadCommand;
import eu.MyPvP.knockback.commands.SetupCommand;
import eu.MyPvP.knockback.commands.ShopCommand;
import eu.MyPvP.knockback.commands.StartCommand;
import eu.MyPvP.knockback.commands.StatsCommand;
import eu.MyPvP.knockback.inventorys.InventoryOrder;
import eu.MyPvP.knockback.inventorys.ShopInventory;
import eu.MyPvP.knockback.listeners.BlockBreakListener;
import eu.MyPvP.knockback.listeners.BlockPlaceListener;
import eu.MyPvP.knockback.listeners.EntityDamageByEntityListener;
import eu.MyPvP.knockback.listeners.EntityDamageListener;
import eu.MyPvP.knockback.listeners.FoodLevelChangeListener;
import eu.MyPvP.knockback.listeners.InventoryClickListener;
import eu.MyPvP.knockback.listeners.InventoryCloseListener;
import eu.MyPvP.knockback.listeners.MapChangeListener;
import eu.MyPvP.knockback.listeners.PlayerDeathListener;
import eu.MyPvP.knockback.listeners.PlayerDropItemListener;
import eu.MyPvP.knockback.listeners.PlayerInteractListener;
import eu.MyPvP.knockback.listeners.PlayerJoinListener;
import eu.MyPvP.knockback.listeners.PlayerMoveListener;
import eu.MyPvP.knockback.listeners.PlayerQuitListener;
import eu.MyPvP.knockback.listeners.PlayerRespawnListener;
import eu.MyPvP.knockback.listeners.ProjectileHitListener;
import eu.MyPvP.knockback.listeners.WeatherChangeListener;
import eu.MyPvP.knockback.mysql.AsyncMySQL;
import eu.MyPvP.knockback.mysql.InventorySlots;
import eu.MyPvP.knockback.mysql.ShopManager;
import eu.MyPvP.knockback.mysql.StatsDaily;
import eu.MyPvP.knockback.mysql.StatsLifetime;
import eu.MyPvP.knockback.mysql.StatsMonthly;
import eu.MyPvP.knockback.tabcomplete.KnockBackTabcomplete;
import eu.MyPvP.knockback.utils.Data;
import eu.MyPvP.knockback.utils.Hologram;
import eu.MyPvP.knockback.utils.KnockBackScore;
import eu.MyPvP.knockback.utils.LocationManager;
import eu.MyPvP.knockback.utils.MapChange;
import eu.MyPvP.knockback.utils.ShopConfig;

/*
* Created by MarvCode at 19.08.2019 20:53:31
*/

public class KnockBack extends JavaPlugin {

	private static KnockBack instance;
	private Data data;
	private LocationManager locationManager;
	private MapChange mapChange;
	private KnockBackScore scoreboard;
	private InventorySlots inventorySlots;
	private ShopInventory shopInventory;
	private StatsLifetime stats;
	private InventoryOrder invOrder;
	private StatsDaily dailyStats;
	private StatsMonthly monthlyStats;
	private ShopConfig shopConfig;
	private ShopManager shopManager;
	private AsyncMySQL asyncMySQL;
	private CoinsAPI coinsAPI = new CoinsAPI();

	public static void main(final String[] args) {

	}

	@Override
	public void onEnable() {
		saveDefaultConfig();

		coinsAPI.connect(getConfig().getString("MySQL.host"), getConfig().getString("MySQL.username"),
				getConfig().getString("MySQL.password"), getConfig().getString("MySQL.database"),
				getConfig().getInt("MySQL.port"));

		asyncMySQL = new AsyncMySQL(this, getConfig().getString("MySQL.host"), getConfig().getInt("MySQL.port"),
				getConfig().getString("MySQL.username"), getConfig().getString("MySQL.password"),
				getConfig().getString("MySQL.database"));
		createTables();
		fetchClasses();
		registerCommands();
		registerListeners();

		getLocationManager().loadLocations();

		if (!getLocationManager().getLocations().isEmpty()) {
			getMapChange().startMapChange();
			getLocationManager().reloadTopHolos();

		}
		startHoloUpdater();
		getShopConfig().createConfig();
		getShopConfig().loadItems();

		for (World w : Bukkit.getWorlds()) {
			w.setGameRuleValue("doDaylightCyle", "false");
			w.setTime(7500);
			w.setThundering(false);
			w.setAutoSave(false);
			w.setStorm(false);
		}

	}

	@Override
	public void onDisable() {
		for (Player player : Bukkit.getOnlinePlayers()) {
			if (!KnockBack.getInstance().getData().getPlayerDatas().containsKey(player.getUniqueId())) {
				player.kickPlayer("�cEs ist ein Fehler aufgetreten.");
				return;
			}
		}
		for (Hologram holo : getLocationManager().getTopKills().values()) {
			holo.destroy();
		}

		for (Hologram holo : getLocationManager().getTopKillsDaily().values()) {
			holo.destroy();
		}
		
		getData().getPlayerDatas().values().forEach(data -> {
			data.uploadAll();
		});
	}

	private void fetchClasses() {
		instance = this;
		data = new Data();
		locationManager = new LocationManager(this);
		mapChange = new MapChange(this);
		scoreboard = new KnockBackScore(this);
		stats = new StatsLifetime(this);
		inventorySlots = new InventorySlots(this);
		invOrder = new InventoryOrder(this);
		dailyStats = new StatsDaily(this);
		shopConfig = new ShopConfig();
		shopInventory = new ShopInventory();
		shopManager = new ShopManager(this);
		monthlyStats = new StatsMonthly(this);
	}

	private void registerCommands() {
		getCommand("setup").setExecutor(new SetupCommand(this));
		getCommand("maplist").setExecutor(new MaplistCommand(this));
		getCommand("start").setExecutor(new StartCommand(this));
		getCommand("coins").setExecutor(new CoinsCommand(this));
		getCommand("inv").setExecutor(new InvCommand(this));
		getCommand("shop").setExecutor(new ShopCommand(this));
		getCommand("knockback").setExecutor(new KnockBackCommand(this));
		getCommand("knockback").setTabCompleter(new KnockBackTabcomplete());
		getCommand("stats").setExecutor(new StatsCommand(this));
		getCommand("build").setExecutor(new BuildCommand(this));
		getCommand("reload").setExecutor(new ReloadCommand());
	}

	private void registerListeners() {
		final PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new PlayerDeathListener(this), this);
		pm.registerEvents(new InventoryCloseListener(this), this);
		pm.registerEvents(new PlayerInteractListener(this), this);
		pm.registerEvents(new FoodLevelChangeListener(this), this);
		pm.registerEvents(new InventoryClickListener(this), this);
		pm.registerEvents(new BlockBreakListener(this), this);
		pm.registerEvents(new BlockPlaceListener(this), this);
		pm.registerEvents(new ProjectileHitListener(this), this);
		pm.registerEvents(new PlayerDropItemListener(this), this);
		pm.registerEvents(new EntityDamageByEntityListener(this), this);
		pm.registerEvents(new PlayerMoveListener(this), this);
		pm.registerEvents(new EntityDamageListener(this), this);
		pm.registerEvents(new PlayerQuitListener(this), this);
		pm.registerEvents(new PlayerRespawnListener(this), this);
		pm.registerEvents(new PlayerJoinListener(this), this);
		pm.registerEvents(new WeatherChangeListener(this), this);
		pm.registerEvents(new MapChangeListener(this), this);
	}

	public static KnockBack getInstance() {
		return instance;
	}

	public Data getData() {
		return data;
	}

	public String getPrefix() {
		return "�cKnockBack �8� �7";
	}

	public String getNoPermissions() {
		return getPrefix() + "�cHierzu hast Du keine Rechte!";
	}

	public LocationManager getLocationManager() {
		return locationManager;
	}

	public MapChange getMapChange() {
		return mapChange;
	}

	public ShopConfig getShopConfig() {
		return shopConfig;
	}

	public KnockBackScore getScoreboard() {
		return scoreboard;
	}

	public StatsLifetime getStats() {
		return stats;
	}

	public StatsDaily getDailyStats() {
		return dailyStats;
	}

	public StatsMonthly getMonthlyStats() {
		return monthlyStats;
	}

	public ShopManager getShopManager() {
		return shopManager;
	}

	public InventorySlots getInventorySlots() {
		return inventorySlots;
	}

	public InventoryOrder getInvOrder() {
		return invOrder;
	}

	public ShopInventory getShopInventory() {
		return shopInventory;
	}

	public CoinsAPI getCoinsAPI() {
		return coinsAPI;
	}

	private void startHoloUpdater() {
		new BukkitRunnable() {
			@Override
			public void run() {
				for (World w : Bukkit.getWorlds()) {
					w.setTime(7500);
					w.setThundering(false);
					w.setStorm(false);
					w.setAutoSave(false);
				}

				getLocationManager().reloadTopHolos();

			}

		}.runTaskTimer(this, 0, 60 * 20);

		new BukkitRunnable() {

			@Override
			public void run() {
				Bukkit.getServer().getOnlinePlayers().forEach(p -> {
					getLocationManager().sendHolos(p);
				});

			}
		}.runTaskLater(this, 70 * 20);
	}

	public AsyncMySQL getAsyncMySQL() {
		return asyncMySQL;
	}

	private void createTables() {
		getAsyncMySQL().update(
				"CREATE TABLE IF NOT EXISTS unlocked_knockback (itemIndex varchar(60), uuid varchar(100), itemType varchar(60), itemId varchar(60), selectedItem varchar(60), purchaseDate varchar(100))");

		getAsyncMySQL().update(
				"CREATE TABLE IF NOT EXISTS stats_knockback (UUID varchar(100), Kills bigInt(64), Deaths bigInt(64), PlayTime bigInt(255), lastPlayed varchar(64))");
		getAsyncMySQL().update("CREATE TABLE IF NOT EXISTS coinsystem_users (uuid varchar(100), coins bigInt(64))");
		getAsyncMySQL().update(
				"CREATE TABLE IF NOT EXISTS stats_knockback_day (UUID varchar(100), Kills bigInt(64), Deaths bigInt(64), PlayTime bigInt(255))");
		getAsyncMySQL().update(
				"CREATE TABLE IF NOT EXISTS stats_knockback_month (UUID varchar(100), Kills bigInt(64), Deaths bigInt(64), PlayTime bigInt(255))");
		getAsyncMySQL().update(
				"CREATE TABLE IF NOT EXISTS inventory_knockback (UUID varchar(100), Arrow int(10), Bow int(10), Stick int(10), Block int(10), Enderpearl int(10), Cobweb int(10))");
		getAsyncMySQL().update(
				"CREATE TABLE IF NOT EXISTS stats_knockback (UUID varchar(100), Kills bigInt(64), Deaths bigInt(64), PlayTime bigInt(255), lastPlayed varchar(64))");
		getAsyncMySQL().update(
				"CREATE TABLE IF NOT EXISTS stats_knockback (UUID varchar(100), Kills bigInt(64), Deaths bigInt(64), PlayTime bigInt(255), lastPlayed varchar(64))");
	}

}
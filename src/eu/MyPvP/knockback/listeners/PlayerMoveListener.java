package eu.MyPvP.knockback.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import eu.MyPvP.knockback.KnockBack;

/*
* Created by MarvCode at 21.08.2019 22:31:51
*/

public class PlayerMoveListener implements Listener {
	private KnockBack plugin;

	public PlayerMoveListener(final KnockBack plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onMove(final PlayerMoveEvent e) {
		Player p = e.getPlayer();
		if (plugin.getData().getBuildMode().contains(p))
			return;
		
		if (plugin.getLocationManager().getDeathHighs()
				.get(plugin.getMapChange().getCurrentMap()) > p.getLocation().getY()) {

			p.setHealth(0D);
			p.spigot().respawn();
		}
	}
}
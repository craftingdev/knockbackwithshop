package eu.MyPvP.knockback.utils;

import java.util.List;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemCreator {

	private static ItemStack itemStack;
	private static ItemMeta itemMeta;

	public ItemCreator(Material material) {
		itemStack = new ItemStack(material);
		itemMeta = itemStack.getItemMeta();
	}

	public ItemCreator(Material material, final int amount, final int subid) {
		itemStack = new ItemStack(material, amount, (short) subid);
		itemMeta = itemStack.getItemMeta();
	}

	@SuppressWarnings("deprecation")
	public ItemCreator(Integer amount, DyeColor color) {
		itemStack = new ItemStack(Material.INK_SACK, amount, color.getData());
		itemMeta = itemStack.getItemMeta();
	}
	
	public ItemCreator setDisplayname(String displayname) {
		itemMeta.setDisplayName(displayname);
		return this;
	}

	public ItemCreator setLore(List<String> loretext) {
		itemMeta.setLore(loretext);

		return this;
	}

	public ItemCreator setDurability(final short i) {
		itemStack.setDurability(i);
		
		return this;
	}
	public ItemCreator addEnchantment(Enchantment enchantment, Integer level, boolean b) {
		itemMeta.addEnchant(enchantment, level, b);
		return this;
	}

	public ItemCreator setUnbreakable(boolean b) {
		itemMeta.spigot().setUnbreakable(b);

		return this;
	}

	public ItemCreator addItemFlags(ItemFlag flag) {
		itemMeta.addItemFlags(flag);
		return this;
	}

	public ItemCreator setAmount(Integer amount) {
		itemStack.setAmount(amount);
		return this;
	}

	public ItemStack build() {
		itemStack.setItemMeta(itemMeta);
		return itemStack;
	}

}

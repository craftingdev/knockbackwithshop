package eu.MyPvP.knockback.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

import eu.MyPvP.knockback.KnockBack;

/*
* Created by MarvCode at 09.09.2019 18:05:42
*/

@SuppressWarnings("unused")
public class AsyncMySQL {
	private ExecutorService executor;
	private MySQL sql;
	private KnockBack plugin;

	public AsyncMySQL(final KnockBack plugin, final String host, final int port, final String user,
			final String password, final String database) {
		try {
			this.plugin = plugin;
			sql = new MySQL(host, port, user, password, database);

			executor = Executors.newCachedThreadPool();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void update(final String statement) {
		executor.submit(() -> sql.queryUpdate(statement));
	}

	public void query(final String statement, final Consumer<ResultSet> consumer, Object... args) {
		executor.submit(() -> sql.query(statement, consumer, args));

	}

	public PreparedStatement prepare(final String query) {
		try {
			return sql.getConnection().prepareStatement(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public MySQL getMySQL() {
		return sql;
	}

	public static class MySQL {

		private String host, user, password, database;
		private int port;

		private Connection conn;

		public MySQL(final String host, final int port, final String user, final String password, final String database)
				throws Exception {
			this.host = host;
			this.port = port;
			this.user = user;
			this.password = password;
			this.database = database;

			this.openConnection();

		}

		public void queryUpdate(final String query, Object... args) {
			checkConnection();
			try (PreparedStatement statement = conn.prepareStatement(query)) {
				for (int i = 0; i < args.length; i++)
					statement.setObject(i + 1, args[i]);
				statement.executeUpdate();
			} catch (SQLException e) {
				throw new DatabaseException(e);
			}
		}

		public void query(final String query, final Consumer<ResultSet> callback, Object... args) {
			checkConnection();
			try (PreparedStatement statement = conn.prepareStatement(query)) {
				for (int i = 0; i < args.length; i++)
					statement.setObject(i + 1, args[i]);
				callback.accept(statement.executeQuery());
			} catch (SQLException e) {
				throw new DatabaseException(e);
			}
		}

		public Connection getConnection() {
			return this.conn;
		}

		private void checkConnection() {
			try {
				if (this.conn == null || !this.conn.isValid(10))
					openConnection();
			} catch (SQLException e) {
				throw new DatabaseException(e);
			}
		}

		public Connection openConnection() {

			try {
				Class.forName("com.mysql.jdbc.Driver");
				return conn = DriverManager.getConnection(
						"jdbc:mysql://" + host + ":" + port + "/" + database + "?rewriteBatchedStatements=true", user,
						password);
			} catch (ClassNotFoundException | SQLException e) {
				throw new DatabaseException(e);
			}

		}

		public void closeConnection() {
			try {
				this.conn.close();
			} catch (SQLException e) {
				throw new DatabaseException(e);
			} finally {
				this.conn = null;
			}
		}
	}
}

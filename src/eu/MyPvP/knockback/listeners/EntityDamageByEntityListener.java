package eu.MyPvP.knockback.listeners;

import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import eu.MyPvP.knockback.KnockBack;
import eu.MyPvP.knockback.utils.ItemCreator;

/*
* Created by MarvCode at 22.08.2019 15:37:06
*/

public class EntityDamageByEntityListener implements Listener {
	private KnockBack plugin;

	public EntityDamageByEntityListener(final KnockBack plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onDamage(final EntityDamageByEntityEvent e) {
		if (e.getEntity().getLocation().getY() >= plugin.getLocationManager().getSpawnHighs()
				.get(plugin.getMapChange().getCurrentMap())
				&& e.getDamager().getLocation().getY() >= plugin.getLocationManager().getSpawnHighs()
						.get(plugin.getMapChange().getCurrentMap())) {
			e.setCancelled(true);
			e.getDamager().sendMessage(plugin.getPrefix() + "�cAm Spawn darfst Du nicht schlagen.");

		}

		if (e.getDamager() != null) {
			if (e.getDamager() instanceof Arrow && e.getEntity() instanceof Player) {
				e.setDamage(0);
				final Arrow arrow = (Arrow) e.getDamager();

				if (arrow.getShooter() instanceof Player && arrow.getShooter() != null) {
					final Player p = (Player) arrow.getShooter();
					plugin.getInventorySlots().getArrow(p.getUniqueId(), slot -> {

						p.getInventory().setItem(slot, new ItemCreator(Material.ARROW).setAmount(1).build());
					});
				}
			}

		}
	}
}
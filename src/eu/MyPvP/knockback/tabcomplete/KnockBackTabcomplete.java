package eu.MyPvP.knockback.tabcomplete;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

/*
* Created by CraftingDev at 23.09.2019 13:57:58
*/

public class KnockBackTabcomplete implements TabCompleter {
	@Override
	public List<String> onTabComplete(final CommandSender s, final Command c, final String l, final String[] a) {
		if (a.length == 1) {
			final List<String> completions = new ArrayList<>();
			completions.add("help");
			completions.add("commands");
			completions.add("reloadholograms");
			completions.add("info");
			Collections.sort(completions);
			return completions;
		}
		
		if (a.length == 2) {
			if(a[0].equalsIgnoreCase("fix")) {
				final List<String> completions = new ArrayList<>();
				completions.add("all");
				
				Bukkit.getOnlinePlayers().forEach(current -> {
					completions.add(current.getName());
				});
				
				Collections.sort(completions);
				return completions;
			}
			
			
		}
		return null;
	}

}
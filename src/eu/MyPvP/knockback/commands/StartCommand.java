package eu.MyPvP.knockback.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import eu.MyPvP.knockback.KnockBack;

/*
* Created by MarvCode at 21.08.2019 08:23:01
*/

public class StartCommand implements CommandExecutor {
	private KnockBack plugin;

	public StartCommand(final KnockBack plugin) {
		this.plugin = plugin;
	}

	
	@Override
	public boolean onCommand(CommandSender s, Command c, String l, String[] a) {
		if (!s.hasPermission("knockback.start")) {
			s.sendMessage(plugin.getNoPermissions());
			return true;
		}
		if (plugin.getMapChange().getSeconds() <= 5) {
			s.sendMessage(plugin.getPrefix()
					+ "�7Du kannst den �eMapwechsel �7nicht verk�rzen, da er gleich ausgef�hrt wird.");
			return true;
		}

		plugin.getMapChange().setSeconds(5);
		s.sendMessage(plugin.getPrefix() + "�7Du hast den �eMapwechsel �7verk�rzt.");
		return false;
	}

}
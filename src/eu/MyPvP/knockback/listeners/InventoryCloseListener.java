package eu.MyPvP.knockback.listeners;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.scheduler.BukkitRunnable;

import eu.MyPvP.knockback.KnockBack;
import eu.MyPvP.knockback.utils.PlayerInventory;

/*
* Created by MarvCode at 23.08.2019 07:47:21
*/

public class InventoryCloseListener implements Listener {
	private KnockBack plugin;

	public InventoryCloseListener(final KnockBack plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onClose(final InventoryCloseEvent e) {
		final Player p = (Player) e.getPlayer();

		int slots;
		if (e.getInventory().getName().equals("§cInventar §8» §7Sortierung")) {
			slots = 0;

			for (int i = 0; i != 9; i++) {
				if (e.getInventory().getItem(i) != null) {
					slots++;
				}
			}

			if (slots != 6) {
				p.sendMessage(plugin.getPrefix() + "§cDein Inventar konnte nicht gespeichert werden!");
				PlayerInventory.giveInventory(p);
				p.playSound(p.getLocation(), Sound.ANVIL_BREAK, 10, 10);
				return;

			} else {
				for (int i = 0; i != 9; i++) {
					if (e.getInventory().getItem(i) == null)
						continue;
					if (e.getInventory().getItem(i).getType() == Material.ARROW) {
						plugin.getInventorySlots().setArrow(p.getUniqueId(), i);

					}

					if (e.getInventory().getItem(i).getType() == Material.BOW) {
						plugin.getInventorySlots().setBow(p.getUniqueId(), i);

					}

					if (e.getInventory().getItem(i).getType() == plugin.getData().getPlayerDatas().get(p.getUniqueId())
							.getSelectedBlock().getItem().getType()) {
						plugin.getInventorySlots().setStone(p.getUniqueId(), i);
					}

					if (e.getInventory().getItem(i).getType() == Material.WEB) {
						plugin.getInventorySlots().setCobweb(p.getUniqueId(), i);
					}

					if (e.getInventory().getItem(i).getType() == plugin.getData().getPlayerDatas().get(p.getUniqueId())
							.getSelectedStick().getItem().getType()) {
						plugin.getInventorySlots().setStick(p.getUniqueId(), i);
					}

					if (e.getInventory().getItem(i).getType() == Material.ENDER_PEARL) {
						plugin.getInventorySlots().setEnderpearl(p.getUniqueId(), i);
					}

				}
				p.sendMessage(plugin.getPrefix() + "§aDein Inventar wurde gespeichert!");
				p.playSound(p.getLocation(), Sound.ANVIL_USE, 10, 10);
				new BukkitRunnable() {

					@Override
					public void run() {
						PlayerInventory.giveInventory(p);

					}
				}.runTaskLaterAsynchronously(plugin, 10);

			}

		}

	}

}

package eu.MyPvP.knockback.mysql;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

import eu.MyPvP.knockback.KnockBack;
import eu.MyPvP.knockback.utils.PlayerData;

/*
* Created by MarvCode at 21.08.2019 13:13:25
*/

public class StatsLifetime {
	private KnockBack plugin;

	public StatsLifetime(final KnockBack plugin) {
		this.plugin = plugin;
	}

	public void isUserExisting(final UUID uuid, final Consumer<Boolean> consumer) {
		plugin.getAsyncMySQL().query("SELECT UUID FROM stats_knockback WHERE UUID = '" + uuid + "'", exists -> {
			try {
				consumer.accept(exists.next());
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}

	public void createPlayer(final UUID uuid) {
		plugin.getAsyncMySQL().update("INSERT INTO stats_knockback (UUID, Kills, Deaths, PlayTime) VALUES ('" + uuid
				+ "','0','0','0') ON DUPLICATE KEY UPDATE UUID = UUID");
	}

	public static class PlayerKillEntry {
		private final UUID uuid;
		private final int kills;

		public PlayerKillEntry(final UUID uuid, final int kills) {
			this.uuid = uuid;
			this.kills = kills;
		}

		public int getKills() {
			return kills;
		}

		public UUID getUUID() {
			return uuid;
		}
	}

	public void getTopKills(final Consumer<List<PlayerKillEntry>> consumer) {
		if (!plugin.getData().getPlayerDatas().isEmpty()) {
			for (PlayerData data : plugin.getData().getPlayerDatas().values()) {
				data.upload();
			}
		}

		plugin.getAsyncMySQL().query("SELECT * FROM stats_knockback ORDER BY (Kills) DESC LIMIT 10", result -> {
			final List<PlayerKillEntry> entries = new ArrayList<>();

			try {
				while (result.next()) {
					entries.add(new PlayerKillEntry(UUID.fromString(result.getString("UUID")), result.getInt("Kills")));
				}

			} catch (SQLException e) {
				throw new DatabaseException(e);
			}

			consumer.accept(entries);
		});

	}

	public void getRanking(final UUID uuid, final Consumer<Integer> consumer) {
		if (plugin.getData().getPlayerDatas().get(uuid) != null) {
			consumer.accept(plugin.getData().getPlayerDatas().get(uuid).getKills());
			return;
		}

		isUserExisting(uuid, exists -> {
			if (exists) {
				plugin.getAsyncMySQL().query(
						"SELECT count(*) AS 'position' FROM 'stats_knockback' WHERE 'Kills' > (SELECT 'Kills' FROM 'stats_knockback' WHERE 'UUID'='"
								+ uuid + "'",
						position -> {
							try {
								if (position.next()) {
									consumer.accept(Integer.valueOf(position.getInt("position") + 1));
								}
							} catch (SQLException e) {
								e.printStackTrace();
							}
						});
			}
		});

	}

	public void getKills(UUID uuid, final Consumer<Integer> consumer) {
		if (plugin.getData().getPlayerDatas().get(uuid) != null) {
			consumer.accept(plugin.getData().getPlayerDatas().get(uuid).getKills());
			return;
		}

		plugin.getAsyncMySQL().query("SELECT Kills FROM stats_knockback WHERE UUID = '" + uuid + "'", kills -> {
			try {
				if (kills.next()) {
					consumer.accept(Integer.valueOf(kills.getInt("Kills")));
				} else {
					consumer.accept(0);
				}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}

	public void getDeaths(UUID uuid, final Consumer<Integer> consumer) {
		if (plugin.getData().getPlayerDatas().get(uuid) != null) {
			consumer.accept(plugin.getData().getPlayerDatas().get(uuid).getDeaths());
			return;
		}

		plugin.getAsyncMySQL().query("SELECT Deaths FROM stats_knockback WHERE UUID = '" + uuid + "'", deaths -> {
			try {
				if (deaths.next()) {
					consumer.accept(Integer.valueOf(deaths.getInt("Deaths")));
				} else {
					consumer.accept(0);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		});
	}

	public void getPlaytime(UUID uuid, final Consumer<Integer> consumer) {
		plugin.getAsyncMySQL().query("SELECT PlayTime FROM stats_knockback WHERE UUID = '" + uuid + "'", playTime -> {
			try {
				if (playTime.next()) {
					consumer.accept(Integer.valueOf(playTime.getInt("PlayTime")));
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}

	public void addKD(UUID uuid, int kills, int deaths) {
		this.getKills(uuid, fetchedKills -> {
			int currentValue = fetchedKills;

			this.getDeaths(uuid, fetchedDeaths -> {
				int currentDeaths = fetchedDeaths;

				isUserExisting(uuid, exists -> {
					if (exists) {
						plugin.getAsyncMySQL().update("UPDATE stats_knockback SET Kills = '" + (currentValue + kills)
								+ "', Deaths = '" + (currentDeaths + deaths) + "' WHERE UUID = '" + uuid + "'");
					}
				});

			});
		});

	}

	public void removeKills(UUID uuid, int value) {
		this.getKills(uuid, fetchedKills -> {
			int currentValue = fetchedKills;

			isUserExisting(uuid, exists -> {
				if (exists) {
					plugin.getAsyncMySQL().update("UPDATE stats_knockback SET Kills = '" + (currentValue - value)
							+ "' WHERE UUID = '" + uuid + "'");
				}
			});
		});
	}

	public void removeDeath(UUID uuid, int value) {
		this.getDeaths(uuid, fetchedDeaths -> {
			int currentDeaths = fetchedDeaths;
			isUserExisting(uuid, exists -> {
				if (exists) {
					plugin.getAsyncMySQL().update("UPDATE stats_knockback SET Deaths = '" + (currentDeaths - value)
							+ "' WHERE UUID = '" + uuid + "'");
				} else {
					createPlayer(uuid);
					removeDeath(uuid, currentDeaths);
				}
			});
		});
	}

	public void setKills(UUID uuid, int value) {
		isUserExisting(uuid, exists -> {
			if (exists) {
				plugin.getAsyncMySQL()
						.update("UPDATE stats_knockback SET Kills = '" + value + "' WHERE UUID = '" + uuid + "'");

			} else {
				createPlayer(uuid);
				setKills(uuid, value);
			}

		});
	}

	public void setPlaytime(UUID uuid, double value) {
		isUserExisting(uuid, exists -> {
			if (exists) {
				plugin.getAsyncMySQL()
						.update("UPDATE stats_knockback SET PlayTime = '" + value + "' WHERE UUID = '" + uuid + "'");

			} else {
				createPlayer(uuid);
				setPlaytime(uuid, value);
			}

		});
	}

	public void setDeaths(UUID uuid, int value) {

		isUserExisting(uuid, exists -> {
			if (exists) {
				plugin.getAsyncMySQL()
						.update("UPDATE stats_knockback SET Deaths = '" + value + "' WHERE UUID = '" + uuid + "'");
			}
		});

	}
}
package eu.MyPvP.knockback.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import eu.MyPvP.knockback.KnockBack;
import ru.tehkode.permissions.bukkit.PermissionsEx;

/*
* Created by MarvCode at 20.08.2019 07:20:01
*/

public class LocationManager {
	private KnockBack plugin;

	public LocationManager(final KnockBack plugin) {
		this.plugin = plugin;
	}

	private final File file = new File("plugins//KnockBack//locations.yml");
	private final YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
	private HashMap<String, Location> locations = new HashMap<String, Location>();
	private HashMap<String, Double> spawnHighs = new HashMap<String, Double>();
	private HashMap<String, Double> deathHighs = new HashMap<String, Double>();
	private HashMap<String, Hologram> topKills = new HashMap<String, Hologram>();
	private ArrayList<Hologram> holograms = new ArrayList<Hologram>();
	private HashMap<String, Hologram> topDailyKills = new HashMap<String, Hologram>();

	public HashMap<String, Location> getLocations() {
		return locations;
	}

	public void setSpawn(final Location loc, final String name) {
		cfg.set(name + ".Worldname", loc.getWorld().getName());
		cfg.set(name + ".X", Double.valueOf(loc.getX()));
		cfg.set(name + ".Y", Double.valueOf(loc.getY() + 0.5));
		cfg.set(name + ".Z", Double.valueOf(loc.getZ()));
		cfg.set(name + ".Yaw", Float.valueOf(loc.getYaw()));
		cfg.set(name + ".Pitch", Float.valueOf(loc.getPitch()));

		try {
			cfg.save(file);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}

	public void setDeathHigh(final Location loc, final String name) {
		cfg.set(name + "_DeathHigh.High", Double.valueOf(loc.getY()));
		cfg.set(name + "_DeathHigh.Worldname", loc.getWorld().getName());

		try {
			cfg.save(file);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}

	public void setSpawnHigh(final Location loc, final String name) {
		cfg.set(name + "_SpawnHigh.Worldname", loc.getWorld().getName());
		cfg.set(name + "_SpawnHigh.High", Double.valueOf(loc.getY()));

		try {
			cfg.save(file);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}

	public void loadLocations() {
		if (cfg.getKeys(true).isEmpty())
			return;

		for (String locationName : cfg.getKeys(false)) {
			String[] splitted = locationName.split("_");
			if (getLocation(splitted[0]) != null) {
				if (locationName.contains("_SpawnHigh")) {
					if (getSpawnHigh(splitted[0]) != null) {
						getSpawnHighs().put(splitted[0], getSpawnHigh(splitted[0]));

						plugin.getLogger().info(plugin.getPrefix() + "§aDie Spawn-Höhe für den Standort §c"
								+ splitted[0] + " §awurde geladen!");

					} else {
						plugin.getLogger().warning(plugin.getPrefix() + "§cEs wurden keine Höhen gefunden.");
					}
				} else if (locationName.contains("_DeathHigh")) {
					if (getDeathHigh(splitted[0]) != null) {

						getDeathHighs().put(splitted[0], getDeathHigh(splitted[0]));

						plugin.getLogger().info(plugin.getPrefix() + "§aDie Todes-Höhe für den Standort §c"
								+ splitted[0] + " §awurde geladen!");
					}
				} else if (locationName.contains("_TopKills")) {
				} else if (locationName.contains("_TopKillsDay")) {

				} else {
					if (getLocation(locationName) != null) {
						getLocations().put(locationName, getLocation(locationName));
						plugin.getLogger()
								.info(plugin.getPrefix() + "§aDer Standort §c" + locationName + " §awurde geladen!");
					} else {
						plugin.getLogger().warning(plugin.getPrefix() + "§7cEs wurden keine Maps gefunden.");
					}
				}

			}
		}

		Object[] keys = getLocations().keySet().toArray();
		plugin.getMapChange().setCurrentMap(plugin.getData().getKey(plugin.getLocationManager().getLocations(),
				plugin.getLocationManager().getLocations().get(keys[0])));
		Bukkit.broadcastMessage(plugin.getPrefix() + "§7Die Map wurde auf §e" + plugin.getMapChange().getCurrentMap()
				+ " §7gewechselt.");
	}

	@SuppressWarnings("deprecation")
	public void reloadTopHolos() {
		getHolograms().clear();

		plugin.getDailyStats().getTopKills(topKills -> {
			List<String> lines = new ArrayList<>();

			lines.add("§8➽ §eTop §7tägliche §eKills");
			topKills.forEach(line -> lines.add("§7"
					+ ChatColor.translateAlternateColorCodes('&',
							PermissionsEx.getPermissionManager().getUser(line.getUUID()).getGroups()[0]
									.getOption("color"))
					+ UUIDFetcher.getName(line.getUUID()) + " §7- §e" + line.getKills() + " §7(§8#" + lines.size()
					+ "§7)"));
			for (int i = topKills.size(); i < 10; i++) {
				lines.add("§c✖ §7(§8#" + lines.size() + "§7)");
			}
			Hologram holo = new Hologram(getHoloKillsDayLocation(plugin.getMapChange().getCurrentMap()), lines);
			getTopKillsDaily().put(plugin.getMapChange().getCurrentMap(), holo);

			if (!getHolograms().contains(holo)) {
				getHolograms().add(holo);
			}

			plugin.getLogger().info("TopDailyKills-Hologram loaded");

		});
		plugin.getStats().getTopKills(topKills -> {
			List<String> lines = new ArrayList<>();
			lines.add("§8➽ §eTop Kills");
			topKills.forEach(line -> lines.add("§7"
					+ ChatColor.translateAlternateColorCodes('&',
							PermissionsEx.getPermissionManager().getUser(line.getUUID()).getGroups()[0]
									.getOption("color"))
					+ UUIDFetcher.getName(line.getUUID()) + " §7- §e" + line.getKills() + " §7(§8#" + lines.size()
					+ "§7)"));
			for (int i = topKills.size(); i < 10; i++) {
				lines.add("§c✖ §7(§8#" + lines.size() + "§7)");
			}

			Hologram holo = new Hologram(getHoloKillsLocation(plugin.getMapChange().getCurrentMap()), lines);
			getTopKills().put(plugin.getMapChange().getCurrentMap(), holo);

			if (!getHolograms().contains(holo)) {
				getHolograms().add(holo);
			}

			plugin.getLogger().info("TopKills-Hologram loaded");
		});

	}

	public void sendHolos(final Player p) {
		getHolograms().forEach(current -> {
			current.destroy(p);
		});

		getTopKills().get(plugin.getMapChange().getCurrentMap()).send(p);
		getTopKillsDaily().get(plugin.getMapChange().getCurrentMap()).send(p);
	}

	public Location getLocation(final String locationName) {
		String w = cfg.getString(locationName + ".Worldname");
		double x = cfg.getDouble(locationName + ".X");
		double y = cfg.getDouble(locationName + ".Y");
		double z = cfg.getDouble(locationName + ".Z");
		double yaw = cfg.getDouble(locationName + ".Yaw");
		double pitch = cfg.getDouble(locationName + ".Pitch");
		Location loc = new Location(Bukkit.getWorld(w), x, y, z);
		loc.setYaw((float) yaw);
		loc.setPitch((float) pitch);

		return loc;

	}

	public void setHoloKillsLocation(final Location loc, final String name) {
		cfg.set(name + "_TopKills.Worldname", loc.getWorld().getName());
		cfg.set(name + "_TopKills.X", Double.valueOf(loc.getX()));
		cfg.set(name + "_TopKills.Y", Double.valueOf(loc.getY() + 0.5));
		cfg.set(name + "_TopKills.Z", Double.valueOf(loc.getZ()));
		cfg.set(name + "_TopKills.Yaw", Float.valueOf(loc.getYaw()));
		cfg.set(name + "_TopKills.Pitch", Float.valueOf(loc.getPitch()));

		try {
			cfg.save(file);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}

	public Location getHoloKillsLocation(final String locationName) {
		String w = cfg.getString(locationName + "_TopKills.Worldname");
		double x = cfg.getDouble(locationName + "_TopKills.X");
		double y = cfg.getDouble(locationName + "_TopKills.Y");
		double z = cfg.getDouble(locationName + "_TopKills.Z");
		double yaw = cfg.getDouble(locationName + "_TopKills.Yaw");
		double pitch = cfg.getDouble(locationName + "_TopKills.Pitch");
		Location loc = new Location(Bukkit.getWorld(w), x, y, z);
		loc.setYaw((float) yaw);
		loc.setPitch((float) pitch);

		return loc;

	}

	public void setHoloKillsDayLocation(final Location loc, final String name) {
		cfg.set(name + "_TopKillsDay.Worldname", loc.getWorld().getName());
		cfg.set(name + "_TopKillsDay.X", Double.valueOf(loc.getX()));
		cfg.set(name + "_TopKillsDay.Y", Double.valueOf(loc.getY() + 0.5));
		cfg.set(name + "_TopKillsDay.Z", Double.valueOf(loc.getZ()));
		cfg.set(name + "_TopKillsDay.Yaw", Float.valueOf(loc.getYaw()));
		cfg.set(name + "_TopKillsDay.Pitch", Float.valueOf(loc.getPitch()));

		try {
			cfg.save(file);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}

	public Location getHoloKillsDayLocation(final String locationName) {
		String w = cfg.getString(locationName + "_TopKillsDay.Worldname");
		double x = cfg.getDouble(locationName + "_TopKillsDay.X");
		double y = cfg.getDouble(locationName + "_TopKillsDay.Y");
		double z = cfg.getDouble(locationName + "_TopKillsDay.Z");
		double yaw = cfg.getDouble(locationName + "_TopKillsDay.Yaw");
		double pitch = cfg.getDouble(locationName + "_TopKillsDay.Pitch");
		Location loc = new Location(Bukkit.getWorld(w), x, y, z);
		loc.setYaw((float) yaw);
		loc.setPitch((float) pitch);

		return loc;

	}

	public Double getDeathHigh(final String name) {
		return cfg.getDouble(name + "_DeathHigh.High");
	}

	public Double getSpawnHigh(final String name) {
		return cfg.getDouble(name + "_SpawnHigh.High");
	}

	public HashMap<String, Double> getSpawnHighs() {
		return spawnHighs;
	}

	public HashMap<String, Double> getDeathHighs() {
		return deathHighs;
	}

	public HashMap<String, Hologram> getTopKills() {
		return topKills;
	}

	public HashMap<String, Hologram> getTopKillsDaily() {
		return topDailyKills;
	}

	public ArrayList<Hologram> getHolograms() {
		return holograms;
	}

}
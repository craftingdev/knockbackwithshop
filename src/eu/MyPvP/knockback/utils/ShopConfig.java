package eu.MyPvP.knockback.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;

import eu.MyPvP.knockback.enums.ShopType;

/*
* Created by MarvCode at 16.09.2019 14:15:47
*/

public class ShopConfig {

	final File file = new File("plugins//KnockBack//shop.yml");
	final File ordner = new File("plugins//KnockBack");
	final YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
	final ArrayList<ShopData> stickDatas = new ArrayList<ShopData>();
	final ArrayList<ShopData> blockDatas = new ArrayList<ShopData>();

	public void createConfig() {
		if (!ordner.exists()) {
			ordner.mkdirs();
		}
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		cfg.options().copyDefaults(true);
		cfg.addDefault("Inventory.Block.Size", 45);
		cfg.addDefault("Inventory.Block.Size", 45);

		cfg.addDefault("Block.Sandstein.Slot", 11);
		cfg.addDefault("Block.Sandstein.Material", "SANDSTONE");
		cfg.addDefault("Block.Sandstein.Displayname", "&bSandstein");
		cfg.addDefault("Block.Sandstein.Price", 0);
		cfg.addDefault("Block.Sandstein.Default", true);
		cfg.addDefault("Block.Sandstein.Id", 0);

		cfg.addDefault("Block.roter_Sandstein.Slot", 12);
		cfg.addDefault("Block.roter_Sandstein.Material", "RED_SANDSTONE");
		cfg.addDefault("Block.roter_Sandstein.Displayname", "&cRoter Sandstein");
		cfg.addDefault("Block.roter_Sandstein.Price", 2000);
		cfg.addDefault("Block.roter_Sandstein.Default", false);
		cfg.addDefault("Block.roter_Sandstein.Id", 1);

		cfg.addDefault("Block.Clay.Slot", 13);
		cfg.addDefault("Block.Clay.Material", "STAINED_CLAY");
		cfg.addDefault("Block.Clay.Displayname", "&fWei�er Ton");
		cfg.addDefault("Block.Clay.Price", 4000);
		cfg.addDefault("Block.Clay.Default", false);
		cfg.addDefault("Block.Clay.Id", 2);

		cfg.addDefault("Block.Kohle.Slot", 14);
		cfg.addDefault("Block.Kohle.Material", "COAL_BLOCK");
		cfg.addDefault("Block.Kohle.Displayname", "&8Kohle");
		cfg.addDefault("Block.Kohle.Price", 6000);
		cfg.addDefault("Block.Kohle.Default", false);
		cfg.addDefault("Block.Kohle.Id", 3);

		cfg.addDefault("Block.Prismarin.Slot", 15);
		cfg.addDefault("Block.Prismarin.Material", "PRISMARINE");
		cfg.addDefault("Block.Prismarin.Displayname", "&3Prismarin");
		cfg.addDefault("Block.Prismarin.Price", 8000);
		cfg.addDefault("Block.Prismarin.Default", false);
		cfg.addDefault("Block.Prismarin.Id", 4);

		cfg.addDefault("Block.Eisen.Slot", 21);
		cfg.addDefault("Block.Eisen.Material", "IRON_BLOCK");
		cfg.addDefault("Block.Eisen.Displayname", "&fEisen");
		cfg.addDefault("Block.Eisen.Price", 10000);
		cfg.addDefault("Block.Eisen.Default", false);
		cfg.addDefault("Block.Eisen.Id", 5);

		cfg.addDefault("Block.Lapis.Slot", 22);
		cfg.addDefault("Block.Lapis.Material", "LAPIS_BLOCK");
		cfg.addDefault("Block.Lapis.Displayname", "&1Lapislazuli");
		cfg.addDefault("Block.Lapis.Price", 12500);
		cfg.addDefault("Block.Lapis.Default", false);
		cfg.addDefault("Block.Lapis.Id", 6);

		cfg.addDefault("Block.Gold.Slot", 23);
		cfg.addDefault("Block.Gold.Material", "GOLD_BLOCK");
		cfg.addDefault("Block.Gold.Displayname", "&6Gold");
		cfg.addDefault("Block.Gold.Price", 15000);
		cfg.addDefault("Block.Gold.Default", false);
		cfg.addDefault("Block.Gold.Id", 7);

		cfg.addDefault("Stick.Knueppel.Slot", 11);
		cfg.addDefault("Stick.Knueppel.Material", "STICK");
		cfg.addDefault("Stick.Knueppel.Displayname", "&bSchlagstock");
		cfg.addDefault("Stick.Knueppel.Price", 0);
		cfg.addDefault("Stick.Knueppel.Default", true);
		cfg.addDefault("Stick.Knueppel.Id", 0);

		cfg.addDefault("Stick.Knochen.Slot", 12);
		cfg.addDefault("Stick.Knochen.Material", "BONE");
		cfg.addDefault("Stick.Knochen.Displayname", "&fKauknochen");
		cfg.addDefault("Stick.Knochen.Price", 2000);
		cfg.addDefault("Stick.Knochen.Default", false);
		cfg.addDefault("Stick.Knochen.Id", 1);

		cfg.addDefault("Stick.Feder.Slot", 13);
		cfg.addDefault("Stick.Feder.Material", "FEATHER");
		cfg.addDefault("Stick.Feder.Displayname", "&fFeder");
		cfg.addDefault("Stick.Feder.Price", 4000);
		cfg.addDefault("Stick.Feder.Default", false);
		cfg.addDefault("Stick.Feder.Id", 2);
		
		cfg.addDefault("Stick.FlowerPower.Slot", 14);
		cfg.addDefault("Stick.FlowerPower.Material", "RED_ROSE");
		cfg.addDefault("Stick.FlowerPower.Displayname", "&dFlower Power");
		cfg.addDefault("Stick.FlowerPower.Price", 6000);
		cfg.addDefault("Stick.FlowerPower.Default", false);
		cfg.addDefault("Stick.FlowerPower.Id", 3);

		cfg.addDefault("Stick.Die_Rasur.Slot", 15);
		cfg.addDefault("Stick.Die_Rasur.Material", "SHEARS");
		cfg.addDefault("Stick.Die_Rasur.Displayname", "&bDie Rasur");
		cfg.addDefault("Stick.Die_Rasur.Price", 8000);
		cfg.addDefault("Stick.Die_Rasur.Default", false);
		cfg.addDefault("Stick.Die_Rasur.Id", 4);

		cfg.addDefault("Stick.Goldener_Stick.Slot", 21);
		cfg.addDefault("Stick.Goldener_Stick.Material", "BLAZE_ROD");
		cfg.addDefault("Stick.Goldener_Stick.Displayname", "&6Goldener Stick");
		cfg.addDefault("Stick.Goldener_Stick.Price", 10000);
		cfg.addDefault("Stick.Goldener_Stick.Default", false);
		cfg.addDefault("Stick.Goldener_Stick.Id", 5);

		cfg.addDefault("Stick.Sense.Slot", 22);
		cfg.addDefault("Stick.Sense.Material", "GOLD_HOE");
		cfg.addDefault("Stick.Sense.Displayname", "&6Goldene Sense");
		cfg.addDefault("Stick.Sense.Price", 12500);
		cfg.addDefault("Stick.Sense.Default", false);
		cfg.addDefault("Stick.Sense.Id", 6);

		cfg.addDefault("Stick.Angel.Slot", 23);
		cfg.addDefault("Stick.Angel.Material", "FISHING_ROD");
		cfg.addDefault("Stick.Angel.Displayname", "&cLegend�re Angel");
		cfg.addDefault("Stick.Angel.Price", 15000);
		cfg.addDefault("Stick.Angel.Default", false);
		cfg.addDefault("Stick.Angel.Id", 7);

		this.saveConfig();
	}

	public void loadItems() {

		new Thread(() -> {
			for (String item : this.getConfig().getConfigurationSection("Block").getKeys(false)) {

				ShopData data = new ShopData(item);
				data.setType(ShopType.BLOCK);
				data.setPrice(this.getConfig().getInt("Block." + item + ".Price"));
				data.setSlot(this.getConfig().getInt("Block." + item + ".Slot"));
				data.setShopItem(
						new ItemCreator(Material.valueOf(this.getConfig().getString("Block." + item + ".Material")))
								.setAmount(32)
								.setDisplayname(this.getConfig().getString("Block." + item + ".Displayname").replace("&", "�")).build());

				data.setItem(
						new ItemCreator(Material.valueOf(this.getConfig().getString("Block." + item + ".Material")))
								.setAmount(32)
								.setDisplayname(this.getConfig().getString("Block." + item + ".Displayname").replace("&", "�")).build());
				
				data.setId(this.getConfig().getInt("Block." + item + ".Id"));
				data.setDefault(this.getConfig().getBoolean("Block." + item + ".Default"));
				blockDatas.add(data);
			}
		}).run();

		new Thread(() -> {
			for (String item : this.getConfig().getConfigurationSection("Stick").getKeys(false)) {
				ShopData data = new ShopData(item);
				data.setType(ShopType.STICK);
				data.setPrice(this.getConfig().getInt("Stick." + item + ".Price"));
				data.setSlot(this.getConfig().getInt("Stick." + item + ".Slot"));
				data.setShopItem(
						new ItemCreator(Material.valueOf(this.getConfig().getString("Stick." + item + ".Material")))
								.setAmount(1)
								.setDisplayname(this.getConfig().getString("Stick." + item + ".Displayname").replace("&", "�")).build());
				data.setItem(
						new ItemCreator(Material.valueOf(this.getConfig().getString("Stick." + item + ".Material")))
								.setAmount(1)
								.setDisplayname(this.getConfig().getString("Stick." + item + ".Displayname").replace("&", "�"))
								.addEnchantment(Enchantment.KNOCKBACK, 1, true).build());
				
				data.setDefault(this.getConfig().getBoolean("Stick." + item + ".Default"));
				data.setId(this.getConfig().getInt("Stick." + item + ".Id"));
				stickDatas.add(data);
			}
		}).run();

	}

	public void saveConfig() {
		try {
			cfg.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<ShopData> getBlockDatas() {
		return blockDatas;
	}

	public ArrayList<ShopData> getStickDatas() {
		return stickDatas;
	}

	public Configuration getConfig() {
		return cfg;
	}
}
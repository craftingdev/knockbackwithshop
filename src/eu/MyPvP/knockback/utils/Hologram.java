package eu.MyPvP.knockback.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_8_R3.EntityArmorStand;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_8_R3.PacketPlayOutSpawnEntityLiving;
import net.minecraft.server.v1_8_R3.WorldServer;

/*
* Created by MarvCode at 21.08.2019 13:25:32
*/

public class Hologram {
	private Location location;
	private List<String> lines;
	private double distance_above = -0.3;
	private List<EntityArmorStand> armorstands = new ArrayList<EntityArmorStand>();

	public Hologram(final Location loc, final String... lines) {
		this.location = loc;
		this.lines = Arrays.asList(lines);
	}

	public Hologram(final Location loc, final List<String> lines) {
		this.location = loc;
		this.lines = (ArrayList<String>) lines;
	}

	/**
	 * 
	 * @return Lines of text to display
	 */
	public List<String> getLines() {
		return lines;
	}

	/**
	 * 
	 * @return Location where the hologram should be displayed
	 */
	public Location getLocation() {
		return location;
	}

	/**
	 * 
	 * @param p The @{Player} that should receive the Hologram
	 */
	public void send(final Player p) {
		double y = getLocation().getY();
		for (int i = 0; i <= lines.size() - 1; i++) {
			y = y + distance_above;
			EntityArmorStand eas = getEntityArmorStand(y);
			eas.setCustomName(lines.get(i));
			display(p, eas);
			armorstands.add(eas);
		}
	}

	/**
	 * Kills / hides / destorys the hologram (, also the entity!) for a specific
	 * player
	 */
	public void destroy(final Player p) {
		for (EntityArmorStand eas : armorstands) {
			PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(eas.getId());
			((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
		}
	}

	/**
	 * Kills / hides / destorys the hologram (, also the entity!) for all players
	 */
	public void destroy() {
		for (final Player p : Bukkit.getOnlinePlayers()) {
			destroy(p);
		}
	}

	/**
	 * Broadcast this hologram to the whole server
	 */
	public void broadcast() {
		for (final Player p : Bukkit.getOnlinePlayers()) {
			send(p);
		}
	}

	/**
	 * 
	 * @param players - A list of Players that are able to see this
	 */
	public void broadcast(final List<Player> players) {
		for (final Player p : players) {
			send(p);
		}
	}

	/**
	 * 
	 * @param y the y axis of the hologram
	 * @return an @see EntityArmorStand for further changes
	 */
	private EntityArmorStand getEntityArmorStand(final double y) {
		WorldServer world = ((CraftWorld) getLocation().getWorld()).getHandle();
		EntityArmorStand eas = new EntityArmorStand(world);
		eas.setLocation(getLocation().getX(), y, getLocation().getZ(), 0f, 0f);
		eas.setInvisible(true);
		eas.setGravity(false);
		eas.setCustomNameVisible(true);
		return eas;
	}

	/**
	 * 
	 * @param p   The player that should receive the packet
	 * @param eas @see EntityArmorStand that is used for the current hologram
	 */
	private void display(final Player p, final EntityArmorStand eas) {
		PacketPlayOutSpawnEntityLiving packet = new PacketPlayOutSpawnEntityLiving(eas);
		((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
	}
}
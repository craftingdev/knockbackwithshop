package eu.MyPvP.knockback.mysql;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.function.Consumer;

import org.bukkit.inventory.ItemStack;

import eu.MyPvP.knockback.KnockBack;
import eu.MyPvP.knockback.enums.ShopType;
import eu.MyPvP.knockback.utils.ShopData;

/*
* Created by MarvCode at 23.08.2019 22:58:23
*/

public class ShopManager {
	private KnockBack plugin;

	public ShopManager(final KnockBack plugin) {
		this.plugin = plugin;
		this.asyncMySQL = plugin.getAsyncMySQL();
	}

	private AsyncMySQL asyncMySQL;

	public void hasPurchased(final UUID uuid, final int id, final ShopType type, final Consumer<Boolean> consumer) {
		asyncMySQL.query("SELECT * FROM unlocked_knockback WHERE UUID = '" + uuid + "' AND ITEMTYPE = '"
				+ type.getTypeName() + "' AND itemId = '" + id + "'", purchased -> {
					try {
						if (purchased.next()) {
							consumer.accept(true);
						} else {
							consumer.accept(false);
						}

					} catch (SQLException e) {
						e.printStackTrace();
					}
				});

	}

	public void getSelected(final UUID uuid, final ShopType type, final Consumer<Integer> consumer) {
		asyncMySQL.query("SELECT itemId FROM unlocked_knockback WHERE UUID = '" + uuid
				+ "' AND SELECTEDITEM = '1' AND ITEMTYPE = '" + type.getTypeName() + "'", selected -> {
					try {
						if (selected.next()) {
							consumer.accept(selected.getInt("itemId"));
						} else {
							if(type == ShopType.BLOCK) {
								consumer.accept(2);
							}
							if(type == ShopType.STICK) {
								consumer.accept(1);
							}
						}

					} catch (SQLException e) {
						e.printStackTrace();
					}
				});
	}

	public void setSelected(final UUID uuid, final ShopType type, final Integer id) {
		asyncMySQL.update("UPDATE unlocked_knockback SET selectedItem = '" + 0 + "' WHERE UUID = '" + uuid
				+ "' AND ITEMTYPE = '" + type.getTypeName() + "'");
		asyncMySQL.update("UPDATE unlocked_knockback SET selectedItem = '" + 1 + "' WHERE UUID = '" + uuid
				+ "' AND ITEMTYPE = '" + type.getTypeName() + "' AND itemId = '" + id + "'");
	}

	public void addPurchase(final UUID uuid, final ShopType type, final Integer id) {
		getIndex(index -> {

			asyncMySQL.update(
					"INSERT INTO unlocked_knockback (ITEMINDEX, UUID, ITEMTYPE, itemId, SELECTEDITEM, PURCHASEDATE) VALUES ('"
							+ index + "', '" + uuid + "', '" + type.getTypeName() + "', '" + id + "', '0', '"
							+ new SimpleDateFormat("yyyy-MM-dd kk:mm:ss").format(new Date()) + "')");
		});
	}

	public void getIndex(final Consumer<Integer> consumer) {
		plugin.getAsyncMySQL().query("SELECT MAX(itemindex) AS lastEntry FROM unlocked_knockback", index -> {
			try {
				if (index.next()) {
					consumer.accept(index.getInt("lastEntry") + 1);
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}

	public ShopData findById(final Integer id, final ShopType type) {
		if (type == ShopType.STICK) {
			for (ShopData data : plugin.getShopConfig().getStickDatas()) {
				if (data.getId() == id) {
					return data;
				}
			}
		} else if (type == ShopType.BLOCK) {
			for (ShopData data : plugin.getShopConfig().getBlockDatas()) {
				if (data.getId() == id) {
					return data;
				}
			}
		}
		return null;

	}

	public ShopData findByItem(final ItemStack item, final ShopType type) {
		if (type == ShopType.STICK) {
			for (ShopData data : plugin.getShopConfig().getStickDatas()) {
				if (data.getItem() == item) {
					return data;
				}
			}
		} else if (type == ShopType.BLOCK) {
			for (ShopData data : plugin.getShopConfig().getBlockDatas()) {
				if (data.getItem() == item) {
					return data;
				}
			}
		}
		return null;

	}

	public ShopData findByShopItem(final ItemStack item, final ShopType type) {
		if (type == ShopType.STICK) {
			for (ShopData data : plugin.getShopConfig().getStickDatas()) {
				if (data.getShopItem().getItemMeta().getDisplayName().equals(item.getItemMeta().getDisplayName())) {
					return data;
				}
			}
		} else if (type == ShopType.BLOCK) {
			for (ShopData data : plugin.getShopConfig().getBlockDatas()) {
				if (data.getShopItem().getItemMeta().getDisplayName().equals(item.getItemMeta().getDisplayName())) {
					return data;
				}
			}
		}
		return null;

	}
}
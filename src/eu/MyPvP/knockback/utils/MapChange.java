package eu.MyPvP.knockback.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import eu.MyPvP.knockback.KnockBack;
import eu.MyPvP.knockback.ownevents.MapChangeEvent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;

/*
* Created by MarvCode at 20.08.2019 07:33:01
*/

public class MapChange {
	private KnockBack plugin;

	public MapChange(final KnockBack plugin) {
		this.plugin = plugin;
	}

	private int seconds = 1200;
	private String currentMap;
	private int mapNumber = 0;

	public void setCurrentMap(final String currentMap) {
		this.currentMap = currentMap;
		Bukkit.getServer().getPluginManager().callEvent(new MapChangeEvent(currentMap));
	}

	public String getCurrentMap() {
		return currentMap;
	}

	public void startMapChange() {
		plugin.getMapChange()
				.setCurrentMap(plugin.getData().getKey(plugin.getLocationManager().getLocations(),
						plugin.getLocationManager().getLocations()
								.get(plugin.getLocationManager().getLocations().keySet().toArray()[mapNumber + 1])));
		new BukkitRunnable() {
			SimpleDateFormat df = new SimpleDateFormat("mm:ss");

			@Override
			public void run() {

				if (seconds != 0) {
					seconds--;

					for (final Player all : Bukkit.getOnlinePlayers()) {
						setActionBar(all,
								"�7Mapwechsel in �e" + df.format(new Date(seconds * 1000).getTime()) + " �7Minuten!");
					}

				} else {
					seconds = 1200;
					plugin.getMapChange()
							.setCurrentMap(plugin.getData().getKey(plugin.getLocationManager().getLocations(),
									plugin.getLocationManager().getLocations().get(
											plugin.getLocationManager().getLocations().keySet().toArray()[mapNumber])));
					mapNumber++;

					if (plugin.getLocationManager().getLocations().size() <= mapNumber) {
						mapNumber = 0;
					}

				}

			}
		}.runTaskTimer(plugin, 0, 20);
	}

	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}

	public int getSeconds() {
		return seconds;
	}

	private void setActionBar(final Player player, final String msg) {
		CraftPlayer p = (CraftPlayer) player;

		IChatBaseComponent icbc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + msg + "\"}");
		PacketPlayOutChat packet = new PacketPlayOutChat(icbc, (byte) 2);
		(p.getHandle()).playerConnection.sendPacket(packet);
	}

}

package eu.MyPvP.knockback.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import eu.MyPvP.knockback.KnockBack;
import eu.MyPvP.knockback.utils.UUIDFetcher;

public class CoinsCommand implements CommandExecutor {

	private KnockBack plugin;

	public CoinsCommand(final KnockBack plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender s, Command c, String l, String[] a) {
		if (!(s instanceof Player))
			return true;

		final Player p = (Player) s;

		if (a.length == 0) {
			plugin.getCoinsAPI().getCoins(p.getUniqueId(), coins -> {
				p.sendMessage(plugin.getPrefix() + "�7Deine Coins: �e" + coins);
			});

		} else if (a.length == 1) {
			if (!p.hasPermission("knockback.coins.other")) {
				p.sendMessage(plugin.getNoPermissions());
				return true;
			}

			try {
				plugin.getCoinsAPI().getCoins(UUIDFetcher.getUUID(a[0]), coins -> {
					p.sendMessage(plugin.getPrefix() + "�7Coins von �e" + a[0] + "�7: �e" + coins);
				});
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (a.length == 3) {
			if (a[0].equalsIgnoreCase("set")) {
				if (!p.hasPermission("knockback.coins.set")) {
					p.sendMessage(plugin.getNoPermissions());
					return true;
				}

				try {
					plugin.getCoinsAPI().setCoins(UUIDFetcher.getUUID(a[1]), Integer.valueOf(a[2]));
					p.sendMessage(plugin.getPrefix() + "�e" + a[1] + "�7's Coins �7wurden auf �e"
							+ Integer.valueOf(a[2]) + " �7gesetzt.");
				} catch (Exception e) {
					e.printStackTrace();
				}

			} else if (a[0].equalsIgnoreCase("add")) {
				if (!p.hasPermission("knockback.coins.add")) {
					p.sendMessage(plugin.getNoPermissions());
					return true;
				}

				try {
					plugin.getCoinsAPI().addCoins(UUIDFetcher.getUUID(a[1]), Integer.valueOf(a[2]));
					p.sendMessage(plugin.getPrefix() + "�e" + a[1] + "�7 wurden �e"
							+ Integer.valueOf(a[2]) + " �7Coins hinzugef�gt.");
				} catch (Exception e) {
					e.printStackTrace();
				}

			} else if (a[0].equalsIgnoreCase("remove")) {
				if (!p.hasPermission("knockback.coins.remove")) {
					p.sendMessage(plugin.getNoPermissions());
					return true;
				}

				try {
					plugin.getCoinsAPI().removeCoins(UUIDFetcher.getUUID(a[1]), Integer.valueOf(a[2]));
					p.sendMessage(plugin.getPrefix() + "�e" + a[1] + "�7 wurden �e"
							+ Integer.valueOf(a[2]) + " �7Coins entfernt.");
				} catch (Exception e) {
					e.printStackTrace();
				}

			} else {
				p.sendMessage(plugin.getPrefix()
						+ "�7Verwende: /coins (�eSET, ADD, REMOVE�7) (�eSPIELER�7) (�eANZAHL�7)");
			}
		} else {
			p.sendMessage(plugin.getPrefix() + "�7Verwende: /�ecoins");
		}
		return false;
	}

}

package eu.MyPvP.knockback.ownevents;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/*
* Created by MarvCode at 21.08.2019 07:38:55
*/

public class MapChangeEvent extends Event {

	String mapName;

	public MapChangeEvent(final String mapName) {
		this.mapName = mapName;
	}

	private static final HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public String getMapName() {
		return mapName;
	}

}
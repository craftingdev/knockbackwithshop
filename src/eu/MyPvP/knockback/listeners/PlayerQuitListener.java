package eu.MyPvP.knockback.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import eu.MyPvP.knockback.KnockBack;

/*
* Created by MarvCode at 21.08.2019 18:00:22
*/

public class PlayerQuitListener implements Listener {
	private KnockBack plugin;

	public PlayerQuitListener(final KnockBack plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onQuit(final PlayerQuitEvent e) {
		final Player p = e.getPlayer();
		e.setQuitMessage(null);
		if (plugin.getData().getPlayerDatas().containsKey(p.getUniqueId())) {
			plugin.getData().getPlayerDatas().get(p.getUniqueId()).uploadAll();
			plugin.getData().getPlayerDatas().remove(p.getUniqueId());
		}
	}
}

package eu.MyPvP.knockback.commands;

import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import eu.MyPvP.knockback.KnockBack;

/*
* Created by MarvCode at 22.08.2019 16:03:31
*/

public class BuildCommand implements CommandExecutor {

	private KnockBack plugin;

	public BuildCommand(final KnockBack plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender s, Command c, String l, String[] a) {
		if (!(s instanceof Player))
			return true;

		final Player p = (Player) s;

		if (!p.hasPermission("knockback.build")) {
			p.sendMessage(plugin.getNoPermissions());
			return true;
		}

		if (plugin.getData().getBuildMode().contains(p)) {
			plugin.getData().getBuildMode().remove(p);
			p.sendMessage(plugin.getPrefix() + "§7Dein BauModus wurde §edeaktiviert.");
			p.setGameMode(GameMode.SURVIVAL);
		} else {
			plugin.getData().getBuildMode().add(p);
			p.sendMessage(plugin.getPrefix() + "§7Dein BauModus wurde §eaktiviert.");
			p.setGameMode(GameMode.CREATIVE);
		}

		return false;
	}

}
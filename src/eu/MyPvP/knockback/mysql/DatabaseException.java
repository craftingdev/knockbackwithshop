package eu.MyPvP.knockback.mysql;

/*
* Created by CraftingDev at 02.10.2019 16:33:52
*/

public class DatabaseException extends RuntimeException {

	private static final long serialVersionUID = -2469258928037437280L;

	public DatabaseException(Exception cause) {
		super(cause);
	}

}

package eu.MyPvP.knockback.listeners;

import org.bukkit.entity.Arrow;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;

import eu.MyPvP.knockback.KnockBack;

/*
* Created by MarvCode at 23.08.2019 10:58:19
*/

public class ProjectileHitListener implements Listener {
	@SuppressWarnings("unused")
	private KnockBack plugin;

	public ProjectileHitListener(final KnockBack plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onProjectileHit(final ProjectileHitEvent e) {
		if (e.getEntity() instanceof Arrow) {
			e.getEntity().remove();
		}

	}
}
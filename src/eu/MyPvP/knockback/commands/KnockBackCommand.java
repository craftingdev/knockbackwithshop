package eu.MyPvP.knockback.commands;

import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;

import eu.MyPvP.knockback.KnockBack;
import ru.tehkode.permissions.bukkit.PermissionsEx;

/*
* Created by MarvCode at 23.09.2019 13:19:12
*/

public class KnockBackCommand implements CommandExecutor {
	private KnockBack plugin;

	public KnockBackCommand(final KnockBack plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender s, Command c, String l, String[] a) {
		if (!s.hasPermission("knockback.admin")) {
			s.sendMessage(plugin.getNoPermissions());
			return true;
		}

		if (a.length == 1) {
			if (a[0].equalsIgnoreCase("help")) {
				sendHelp(s);
				return true;
			}

			if (a[0].equalsIgnoreCase("commands")) {
				s.sendMessage(plugin.getPrefix() + "�8�m�-----------��7 Befehle �8�m�-----------�");
				for (Entry<String, Map<String, Object>> command : plugin.getDescription().getCommands().entrySet()) {
					s.sendMessage(plugin.getPrefix() + " �8- �7/" + command.getKey());
				}
				s.sendMessage(plugin.getPrefix() + "�8�m�-----------��7 Befehle �8�m�-----------�");
				return true;
			}

			if (a[0].equalsIgnoreCase("reloadholograms")) {
				plugin.getLocationManager().reloadTopHolos();

				Bukkit.getOnlinePlayers().forEach(current -> {
					plugin.getLocationManager().sendHolos(current);
				});

				s.sendMessage(plugin.getPrefix() + "�7Die Hologramme werden nun neugeladen.");
				return true;
			}

			if (a[0].equalsIgnoreCase("info")) {
				s.sendMessage(plugin.getPrefix() + "�8�m�----------------------�");
				s.sendMessage(plugin.getPrefix() + "�7Name: �c" + plugin.getDescription().getName());
				s.sendMessage(plugin.getPrefix() + "�7Version: �a" + plugin.getDescription().getVersion());
				s.sendMessage(plugin.getPrefix() + "�7Author: �a" + plugin.getDescription().getAuthors().get(0));
				s.sendMessage(plugin.getPrefix() + "�7Main: " + plugin.getDescription().getMain());
				s.sendMessage(plugin.getPrefix() + "�8�m�----------------------�");
				return true;
			}
		} else if (a.length == 2) {
			if (a[0].equalsIgnoreCase("fix")) {
				if (a[1].equalsIgnoreCase("all")) {

					s.sendMessage(plugin.getPrefix() + "�7Behebe Fehler von allen Spielern...");
					Bukkit.getOnlinePlayers().forEach(currentPlayer -> {
						Bukkit.getServer().getPluginManager().callEvent(new PlayerJoinEvent(currentPlayer, null));
						currentPlayer.sendMessage(plugin.getPrefix() + "�aEs wurden Fehler von Dir behoben.");
					});
					s.sendMessage(plugin.getPrefix() + "�7Alle Fehler wurden behoben.");

					return true;
				}

				final Player toFix = Bukkit.getPlayer(a[1]);
				if (toFix == null) {
					s.sendMessage(plugin.getPrefix() + "�7Dieser Spieler ist Offline!");
					return true;
				}
				@SuppressWarnings("deprecation")
				final String color = PermissionsEx.getPermissionManager().getUser(toFix.getUniqueId()).getGroups()[0]
						.getOption("color").replace("&", "�");

				s.sendMessage(plugin.getPrefix() + "�7Behebe Fehler von �e" + color + toFix.getName() + "�7...");
				Bukkit.getServer().getPluginManager().callEvent(new PlayerJoinEvent(toFix, null));
				toFix.sendMessage(plugin.getPrefix() + "�aEs wurden Fehler von Dir behoben.");

				s.sendMessage(
						plugin.getPrefix() + "�7Die Fehler von �e" + color + toFix.getName() + " �7wurden behoben.");

				return true;

			}
		} else {
			sendHelp(s);
		}
		return false;
	}

	void sendHelp(final CommandSender s) {
		s.sendMessage(plugin.getPrefix() + "�8�m�-----------��7 Hilfe �8�m�-----------�");
		s.sendMessage(plugin.getPrefix() + " �8- �7/kb commands");
		s.sendMessage(plugin.getPrefix() + " �8- �7/kb info");
		s.sendMessage(plugin.getPrefix() + " �8- �7/kb reloadholograms");
		s.sendMessage(plugin.getPrefix() + " �8- �7/kb fix (�eALL�7, �eSPIELER�7)");
		s.sendMessage(plugin.getPrefix() + "�8�m�-----------��7 Hilfe �8�m�-----------�");
	}

}
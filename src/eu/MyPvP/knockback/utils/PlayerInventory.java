package eu.MyPvP.knockback.utils;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;

import eu.MyPvP.knockback.KnockBack;

/*
* Created by MarvCode at 24.08.2019 14:47:40
*/

public class PlayerInventory {
	private static KnockBack plugin = KnockBack.getInstance();

	public static void giveInventory(final Player p) {
		p.getInventory().clear();
		plugin.getInventorySlots().getArrow(p.getUniqueId(), slot -> {
			p.getInventory().setItem(slot, new ItemCreator(Material.ARROW).setAmount(1).build());
		});

		plugin.getInventorySlots().getBow(p.getUniqueId(), slot -> {
			p.getInventory().setItem(slot, new ItemCreator(Material.BOW).addEnchantment(Enchantment.DURABILITY, 2, true)
					.addItemFlags(ItemFlag.HIDE_ENCHANTS).setAmount(1).setUnbreakable(true).build());
		});
		plugin.getInventorySlots().getStick(p.getUniqueId(), slot -> {
			p.getInventory().setItem(slot,
					plugin.getData().getPlayerDatas().get(p.getUniqueId()).getSelectedStick().getItem());
		});

		plugin.getInventorySlots().getStone(p.getUniqueId(), slot -> {
			p.getInventory().setItem(slot,
					plugin.getData().getPlayerDatas().get(p.getUniqueId()).getSelectedBlock().getItem());
		});

		plugin.getInventorySlots().getCobweb(p.getUniqueId(), slot -> {
			p.getInventory().setItem(slot, new ItemCreator(Material.WEB).setAmount(1).build());
		});

		plugin.getInventorySlots().getEnderpearl(p.getUniqueId(), slot -> {
			p.getInventory().setItem(slot, new ItemCreator(Material.ENDER_PEARL).setAmount(1).build());
		});
	}
}

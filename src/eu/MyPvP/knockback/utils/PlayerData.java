package eu.MyPvP.knockback.utils;

import java.util.UUID;

import org.bukkit.Bukkit;

import eu.MyPvP.knockback.KnockBack;
import eu.MyPvP.knockback.enums.ShopType;

/*
* Created by MarvCode at 19.08.2019 20:57:24
*/

public class PlayerData {
	KnockBack plugin = KnockBack.getInstance();
	UUID uuid;
	Integer kills, deaths, dailyKills, dailyDeaths, monthlyKills, monthlyDeaths;
	double joinMillis;
	String stickName, blockName;
	ShopData selectedBlock, selectedStick;

	public PlayerData(final UUID uuid) {
		this.uuid = uuid;
		plugin.getStats().getKills(uuid, kills -> {
			this.kills = kills;
			if (Bukkit.getPlayer(uuid) != null) {
				plugin.getScoreboard().set(Bukkit.getPlayer(uuid));
			}
		});

		plugin.getStats().getDeaths(uuid, deaths -> {
			this.deaths = deaths;
			if (Bukkit.getPlayer(uuid) != null) {
				plugin.getScoreboard().set(Bukkit.getPlayer(uuid));
			}
		});

		plugin.getDailyStats().getDeaths(uuid, deaths -> {
			this.dailyDeaths = deaths;
		});

		plugin.getDailyStats().getKills(uuid, kills -> {
			this.dailyKills = kills;
		});

		plugin.getMonthlyStats().getDeaths(uuid, deaths -> {
			this.monthlyDeaths = deaths;
		});

		plugin.getMonthlyStats().getKills(uuid, kills -> {
			this.monthlyKills = kills;
		});

	}

	public void addKills() {
		this.kills++;
	}

	public void addDeaths() {
		this.deaths++;
	}

	public void setStickName(String stickName) {
		this.stickName = stickName;
	}

	public void setBlockName(String blockName) {
		this.blockName = blockName;
	}

	public void addDailyKills() {
		this.dailyKills++;
	}

	public void addDailyDeaths() {
		this.dailyDeaths++;
	}

	public Integer getDeaths() {
		return deaths;
	}

	public Integer getKills() {
		return kills;
	}

	public UUID getUUID() {
		return uuid;
	}

	public void setSelectedBlock(ShopData selectedBlock) {
		this.selectedBlock = selectedBlock;
	}

	public void setSelectedStick(ShopData selectedStick) {
		this.selectedStick = selectedStick;
	}

	public ShopData getSelectedBlock() {
		return selectedBlock;
	}

	public ShopData getSelectedStick() {
		return selectedStick;
	}

	public double getJoinMillis() {
		return joinMillis;
	}

	public Integer getDailyDeaths() {
		return dailyDeaths;
	}

	public Integer getDailyKills() {
		return dailyKills;
	}

	public void setJoinMillis(final double joinMillis) {
		this.joinMillis = joinMillis;
	}

	public void upload() {
		plugin.getStats().setDeaths(uuid, this.deaths);
		plugin.getStats().setKills(uuid, this.kills);

	}

	public void uploadAll() {
		if (this.dailyDeaths == null || this.dailyKills == null || this.monthlyDeaths == null
				|| this.monthlyKills == null || this.kills == null || this.deaths == null) {
			plugin.getLogger().warning("�c" + uuid + "'s Daten konnten nicht hochgeladen werden!");
			return;
		}
		plugin.getStats().getPlaytime(uuid, playTime -> {
			plugin.getStats().setPlaytime(uuid, ((System.currentTimeMillis() - this.joinMillis) + playTime));
		});
		plugin.getStats().setDeaths(uuid, this.deaths);
		plugin.getStats().setKills(uuid, this.kills);
		plugin.getDailyStats().getPlaytime(uuid, playTime -> {
			plugin.getDailyStats().setPlaytime(uuid, ((System.currentTimeMillis() - this.joinMillis) + playTime));
		});
		plugin.getDailyStats().setDeaths(uuid, this.dailyDeaths);
		plugin.getDailyStats().setKills(uuid, this.dailyKills);

		plugin.getShopManager().setSelected(uuid, ShopType.STICK, getSelectedStick().getId());
		plugin.getShopManager().setSelected(uuid, ShopType.BLOCK, getSelectedBlock().getId());

		plugin.getMonthlyStats().setDeaths(uuid, this.monthlyDeaths);
		plugin.getMonthlyStats().setKills(uuid, this.monthlyKills);
		plugin.getMonthlyStats().getPlaytime(uuid, playTime -> {
			plugin.getMonthlyStats().setPlaytime(uuid, ((System.currentTimeMillis() - this.joinMillis) + playTime));
		});

	}
}

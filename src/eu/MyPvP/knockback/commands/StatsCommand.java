package eu.MyPvP.knockback.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import eu.MyPvP.knockback.KnockBack;

/*
* Created by MarvCode at 22.08.2019 18:15:01
*/

public class StatsCommand implements CommandExecutor {
	private KnockBack plugin;

	public StatsCommand(final KnockBack plugin) {
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender s, Command c, String l, String[] a) {
		if (!(s instanceof Player))
			return true;

		Player p = (Player) s;

		if (a.length == 0) {
			if (plugin.getData().getPlayerDatas().get(p.getUniqueId()) == null) {
				p.sendMessage(plugin.getPrefix() + "§7Es konnten keine Statistiken zu Dir geladen werden.");
				return true;
			}
			plugin.getStats().getRanking(p.getUniqueId(), position -> {

				double kdr = (double) plugin.getData().getPlayerDatas().get(p.getUniqueId()).getKills()
						/ (double) plugin.getData().getPlayerDatas().get(p.getUniqueId()).getDeaths();
				kdr = (double) Math.round(kdr * 100.0D) / 100.0D;
				p.sendMessage(plugin.getPrefix());
				p.sendMessage(plugin.getPrefix() + "§7Deine Statistiken§8:");
				p.sendMessage(plugin.getPrefix() + "§7Tode: §e"
						+ plugin.getData().getPlayerDatas().get(p.getUniqueId()).getDeaths());
				p.sendMessage(plugin.getPrefix() + "§7Kills: §a"
						+ plugin.getData().getPlayerDatas().get(p.getUniqueId()).getKills());
				p.sendMessage(plugin.getPrefix() + "§7K/D: §c" + kdr);
				p.sendMessage(plugin.getPrefix() + "§7Position: §e" + position);
				p.sendMessage(plugin.getPrefix());

			});

		} else if (a.length == 1) {
			if (a[0].equalsIgnoreCase("day")) {
				if (plugin.getData().getPlayerDatas().get(p.getUniqueId()) == null) {
					p.sendMessage(plugin.getPrefix() + "§7Es konnten keine Statistiken zu Dir geladen werden.");
					return true;
				}
				plugin.getDailyStats().getRanking(p.getUniqueId(), ranking -> {

					double kdr = (double) plugin.getData().getPlayerDatas().get(p.getUniqueId()).getDailyKills()
							/ (double) plugin.getData().getPlayerDatas().get(p.getUniqueId()).getDailyDeaths();
					kdr = (double) Math.round(kdr * 100.0D) / 100.0D;
					p.sendMessage(plugin.getPrefix());
					p.sendMessage(plugin.getPrefix() + "§7Deine täglichen Statistiken§8:");
					p.sendMessage(plugin.getPrefix() + "§7Tode: §e"
							+ plugin.getData().getPlayerDatas().get(p.getUniqueId()).getDailyDeaths());
					p.sendMessage(plugin.getPrefix() + "§7Kills: §a"
							+ plugin.getData().getPlayerDatas().get(p.getUniqueId()).getDailyKills());
					p.sendMessage(plugin.getPrefix() + "§7K/D: §c" + kdr);
					p.sendMessage(plugin.getPrefix() + "§7Position: §e" + ranking);
					p.sendMessage(plugin.getPrefix());
				});
			}

		} else {
			p.sendMessage(plugin.getPrefix() + "§7Verwende: /stats (§eDAY§7)");
		}
		return false;
	}
}
package eu.MyPvP.knockback.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

import eu.MyPvP.knockback.KnockBack;

/*
* Created by MarvCode at 28.08.2019 17:49:06
*/

public class FoodLevelChangeListener implements Listener{
	@SuppressWarnings("unused")
	private KnockBack plugin;

	public FoodLevelChangeListener(final KnockBack plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onFoodLevelChange(final FoodLevelChangeEvent e) {
		e.setCancelled(true);
	}
}

package eu.MyPvP.knockback.listeners;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;

import eu.MyPvP.knockback.KnockBack;
import eu.MyPvP.knockback.enums.ShopType;
import eu.MyPvP.knockback.utils.PlayerData;
import eu.MyPvP.knockback.utils.PlayerInventory;
import eu.MyPvP.knockback.utils.ShopData;

/*
* Created by MarvCode at 20.08.2019 07:16:47
*/

public class PlayerJoinListener implements Listener {
	private KnockBack plugin;

	public PlayerJoinListener(final KnockBack plugin) {
		this.plugin = plugin;
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onJoin(final PlayerJoinEvent e) {
		final Player p = e.getPlayer();

		p.setMaxHealth(20);
		e.setJoinMessage(null);

		plugin.getInventorySlots().createPlayer(p.getUniqueId());
		plugin.getStats().createPlayer(p.getUniqueId());
		plugin.getDailyStats().createPlayer(p.getUniqueId());
		plugin.getMonthlyStats().createPlayer(p.getUniqueId());
		for (ShopData data : plugin.getShopConfig().getBlockDatas()) {
			if (data.isDefault()) {
				plugin.getShopManager().hasPurchased(p.getUniqueId(), data.getId(), ShopType.BLOCK, purchased -> {
					if (!purchased) {
						plugin.getShopManager().addPurchase(p.getUniqueId(), ShopType.BLOCK, data.getId());
						plugin.getShopManager().setSelected(p.getUniqueId(), ShopType.BLOCK, data.getId());

					}
				});
			}
		}

		new BukkitRunnable() {
			@Override
			public void run() {
				for (ShopData stickData : plugin.getShopConfig().getStickDatas()) {
					if (stickData.isDefault()) {
						plugin.getShopManager().hasPurchased(p.getUniqueId(), stickData.getId(), ShopType.STICK,
								purchasedStick -> {
									if (!purchasedStick) {
										plugin.getShopManager().addPurchase(p.getUniqueId(), ShopType.STICK,
												stickData.getId());
										plugin.getShopManager().setSelected(p.getUniqueId(), ShopType.STICK,
												stickData.getId());

									}
								});
					}

				}
			}
		}.runTaskLaterAsynchronously(plugin, 10);

		if (plugin.getData().getBuildMode().contains(p)) {
			plugin.getData().getBuildMode().remove(p);
		}

		final PlayerData data = new PlayerData(p.getUniqueId());
		data.setJoinMillis(System.currentTimeMillis());

		new BukkitRunnable() {

			@Override
			public void run() {
				plugin.getShopManager().getSelected(p.getUniqueId(), ShopType.BLOCK, selectedBlock -> {
					data.setSelectedBlock(plugin.getShopManager().findById(selectedBlock, ShopType.BLOCK));
					plugin.getShopManager().getSelected(p.getUniqueId(), ShopType.STICK, selectedStick -> {
						data.setSelectedStick(plugin.getShopManager().findById(selectedStick, ShopType.STICK));
						PlayerInventory.giveInventory(p);
					});
				});
			}
		}.runTaskLaterAsynchronously(plugin, 30);

		plugin.getData().getPlayerDatas().put(p.getUniqueId(), data);

		plugin.getLocationManager().sendHolos(p);

		p.sendTitle("§7Map", "§8● §d" + plugin.getMapChange().getCurrentMap() + " §8●");
		p.resetMaxHealth();
		p.setHealth(20D);
		p.setFoodLevel(24);
		p.setLevel(0);
		p.setGameMode(GameMode.SURVIVAL);
		new BukkitRunnable() {

			@Override
			public void run() {
				p.teleport(plugin.getLocationManager().getLocations().get(plugin.getMapChange().getCurrentMap()));

			}
		}.runTaskLater(plugin, 2);

		plugin.getScoreboard().set(p);
	}
}

package eu.MyPvP.knockback.commands;

import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import eu.MyPvP.knockback.KnockBack;

/*
* Created by MarvCode at 23.08.2019 11:57:05
*/

public class ShopCommand implements CommandExecutor {
	private KnockBack plugin;

	public ShopCommand(final KnockBack plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender s, Command c, String l, String[] a) {
		if (!(s instanceof Player))
			return true;

		final Player p = (Player) s;

		if (p.getLocation().getY() < plugin.getLocationManager().getSpawnHighs()
				.get(plugin.getMapChange().getCurrentMap())) {
			p.sendMessage(plugin.getPrefix() + "�7Du kannst den �eShop �7nur am �eSpawn �7�ffnen.");
			return true;
		}

		p.openInventory(plugin.getShopInventory().getMenu(p.getUniqueId()));
		p.playSound(p.getLocation(), Sound.CHEST_OPEN, 10, 10);

		return false;
	}

}
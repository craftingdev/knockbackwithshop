package eu.MyPvP.knockback.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

import eu.MyPvP.knockback.KnockBack;
import eu.MyPvP.knockback.ownevents.MapChangeEvent;
import eu.MyPvP.knockback.utils.Hologram;
import eu.MyPvP.knockback.utils.PlayerInventory;

/*
* Created by MarvCode at 21.08.2019 08:17:53
*/

public class MapChangeListener implements Listener {
	private KnockBack plugin;

	public MapChangeListener(final KnockBack plugin) {
		this.plugin = plugin;
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onMapChange(final MapChangeEvent e) {
		plugin.getLocationManager().reloadTopHolos();
		new BukkitRunnable() {

			@Override
			public void run() {
				if (!plugin.getLocationManager().getTopKills().isEmpty()) {
					plugin.getLocationManager().getTopKills().get(plugin.getMapChange().getCurrentMap()).broadcast();
				}

				if (!plugin.getLocationManager().getTopKillsDaily().isEmpty()) {
					plugin.getLocationManager().getTopKillsDaily().get(plugin.getMapChange().getCurrentMap())
							.broadcast();
				}
				if (!plugin.getLocationManager().getHolograms().isEmpty()) {
					for (Hologram holo : plugin.getLocationManager().getHolograms()) {
						holo.destroy();
					}
				}

			}
		}.runTaskLater(plugin, 20 * 6);

		for (Player all : Bukkit.getOnlinePlayers()) {
			if (!plugin.getData().getPlayerDatas().containsKey(all.getUniqueId())) {
				all.kickPlayer("§cEs ist ein Fehler aufgetreten.");
				return;
			}

			all.teleport(plugin.getLocationManager().getLocations().get(plugin.getMapChange().getCurrentMap()));
			all.sendTitle("§7Mapwechsel", "§8● §d" + plugin.getMapChange().getCurrentMap() + " §8●");
			plugin.getScoreboard().set(all);

			all.resetMaxHealth();
			all.setHealth(20D);

			PlayerInventory.giveInventory(all);
		}

		Bukkit.broadcastMessage(plugin.getPrefix() + "§7Es wird nun auf der Map §d"
				+ plugin.getMapChange().getCurrentMap() + " §7gespielt.");
	}
}

package eu.MyPvP.knockback.utils;

import org.bukkit.inventory.ItemStack;

import eu.MyPvP.knockback.enums.ShopType;

/*
* Created by MarvCode at 16.09.2019 14:20:40
*/

public class ShopData {

	final String itemName;
	Integer price, slot, id;
	ItemStack item, shopItem;
	boolean isDefault;
	ShopType type;

	public ShopData(final String itemName) {
		this.itemName = itemName;
	}

	public void setPrice(final Integer price) {
		this.price = price;
	}

	public void setItem(final ItemStack item) {
		this.item = item;
	}

	public void setSlot(final Integer slot) {
		this.slot = slot;
	}

	public void setShopItem(final ItemStack shopItem) {
		this.shopItem = shopItem;
	}

	public void setType(final ShopType type) {
		this.type = type;
	}

	public void setId(final Integer id) {
		this.id = id;
	}

	public ItemStack getItem() {
		return item;
	}

	public String getItemName() {
		return itemName;
	}
	
	public boolean isDefault() {
		return isDefault;
	}
	
	public void setDefault(final boolean isDefault) {
		this.isDefault = isDefault;
	}

	public Integer getPrice() {
		return price;
	}

	public Integer getId() {
		return id;
	}

	public Integer getSlot() {
		return slot;
	}

	public ShopType getType() {
		return type;
	}

	public ItemStack getShopItem() {
		return shopItem;
	}
}
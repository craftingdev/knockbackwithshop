package eu.MyPvP.knockback.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.entity.Player;

/*
* Created by MarvCode at 20.08.2019 07:14:49
*/

public class Data {

	private HashMap<UUID, PlayerData> playerDatas = new HashMap<UUID, PlayerData>();
	private ArrayList<Hologram> holograms = new ArrayList<Hologram>();
	private ArrayList<Player> buildMode = new ArrayList<Player>();
	public HashMap<Player, Player> nemesis = new HashMap<Player, Player>();

	public HashMap<UUID, PlayerData> getPlayerDatas() {
		return playerDatas;
	}

	public <K, V> K getKey(final Map<K, V> m, final V v) {
		for (Entry<K, V> e : m.entrySet()) {
			if (e.getValue().equals(v)) {
				return e.getKey();
			}
		}
		return null;
	}

	public ArrayList<Hologram> getHolograms() {
		return holograms;
	}

	public ArrayList<Player> getBuildMode() {
		return buildMode;
	}

	public HashMap<Player, Player> getNemesis() {
		return nemesis;
	}
}

package eu.MyPvP.knockback.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.scheduler.BukkitRunnable;

import eu.MyPvP.knockback.KnockBack;
import eu.MyPvP.knockback.utils.PlayerInventory;

/*
* Created by MarvCode at 21.08.2019 21:39:42
*/

public class PlayerRespawnListener implements Listener {
	private KnockBack plugin;

	public PlayerRespawnListener(final KnockBack plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onRespawn(final PlayerRespawnEvent e) {
		final Player p = e.getPlayer();
		e.setRespawnLocation(plugin.getLocationManager().getLocation(plugin.getMapChange().getCurrentMap()));

		new BukkitRunnable() {

			@Override
			public void run() {
				PlayerInventory.giveInventory(p);
				plugin.getLocationManager().sendHolos(p);

			}
		}.runTaskLater(plugin, 10);
	}
}

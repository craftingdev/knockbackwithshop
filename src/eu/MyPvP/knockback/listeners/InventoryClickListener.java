package eu.MyPvP.knockback.listeners;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import eu.MyPvP.knockback.KnockBack;
import eu.MyPvP.knockback.enums.ShopType;
import eu.MyPvP.knockback.utils.PlayerInventory;
import eu.MyPvP.knockback.utils.ShopData;

/*
* Created by MarvCode at 23.08.2019 12:04:07
*/

public class InventoryClickListener implements Listener {
	private KnockBack plugin;

	public InventoryClickListener(final KnockBack plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onInventoryClick(final InventoryClickEvent e) {
		final Inventory inv = e.getInventory();
		final ItemStack item = e.getCurrentItem();
		final Player p = (Player) e.getWhoClicked();
		if (inv == null)
			return;

		if (inv.getName() == null)
			return;

		if (item == null)
			return;

		if (item.getItemMeta() == null)
			return;

		if (!(e.getRawSlot() < 9)) {
			e.setCancelled(true);
		}

		if (e.getInventory().getName().startsWith("§cInventar §8» §7Sortierung")) {
			if (!(e.getRawSlot() < 9)) {
				e.setCancelled(true);
			}
		}

		if (item.getItemMeta().getDisplayName() == null)
			return;

		if (inv.getName().equalsIgnoreCase("§cShop §8» §eMenü")) {
			e.setCancelled(true);
			if (item.getItemMeta().getDisplayName().equalsIgnoreCase("§8● §eSticks §8●")) {
				p.openInventory(plugin.getShopInventory().getSticks(p.getUniqueId()));
				p.playSound(p.getLocation(), Sound.CLICK, 10, 10);
				return;
			}

			if (item.getItemMeta().getDisplayName().equalsIgnoreCase("§8● §eBlöcke §8●")) {
				p.openInventory(plugin.getShopInventory().getBlocks(p.getUniqueId()));
				p.playSound(p.getLocation(), Sound.CLICK, 10, 10);
				return;
			}

			if (item.getItemMeta().getDisplayName().equalsIgnoreCase("§8● §eInventarsortierung §8●")) {
				p.openInventory(plugin.getInvOrder().getInventory(p.getUniqueId()));
				p.playSound(p.getLocation(), Sound.CLICK, 10, 10);
				return;
			}

			return;
		}

		if (inv.getName().equalsIgnoreCase("§8● §eInventarsortierung §8●")) {
			e.setCancelled(true);
			p.openInventory(plugin.getInvOrder().getInventory(p.getUniqueId()));
			p.playSound(p.getLocation(), Sound.CLICK, 10, 10);
			return;
		}

		if (inv.getName().equalsIgnoreCase("§cShop §8» §eSticks")) {
			e.setCancelled(true);
			if (item.getItemMeta().getDisplayName().equals("§8» §cZurück")) {
				p.openInventory(plugin.getShopInventory().getMenu(p.getUniqueId()));
				p.playSound(p.getLocation(), Sound.CLICK, 10, 10);
				return;
			}

			if (e.getClick().equals(ClickType.RIGHT)) {
				for (ShopData data : plugin.getShopConfig().getStickDatas()) {
					if (item.getItemMeta().getDisplayName().equals(data.getShopItem().getItemMeta().getDisplayName())) {

						p.closeInventory();
						p.playSound(p.getLocation(), Sound.CLICK, 10, 10);
						plugin.getShopManager().hasPurchased(p.getUniqueId(),
								plugin.getShopManager().findByShopItem(item, ShopType.STICK).getId(), ShopType.STICK,
								hasPurchased -> {
									if (hasPurchased) {
										p.sendMessage(plugin.getPrefix() + "§7Du hast den §7"
												+ data.getItem().getItemMeta().getDisplayName()
												+ " §7Stick bereits gekauft.");
										return;
									}

									plugin.getCoinsAPI().hasCoins(p.getUniqueId(), data.getPrice(), hasCoins -> {
										if (!hasCoins) {
											plugin.getCoinsAPI().getCoins(p.getUniqueId(), coins -> {
												p.sendMessage(plugin.getPrefix() + "§7Dir fehlen noch §e"
														+ (data.getPrice() - coins) + " §7Coins.");
											});

											return;
										}
										plugin.getCoinsAPI().removeCoins(p.getUniqueId(), data.getPrice());
										plugin.getShopManager().addPurchase(p.getUniqueId(), ShopType.STICK,
												plugin.getShopManager().findByShopItem(item, ShopType.STICK).getId());
										p.sendMessage(plugin.getPrefix() + "§7Du hast den Stick §8'"
												+ data.getItem().getItemMeta().getDisplayName() + "§8' §7gekauft.");
									});
								});

					}
				}
			} else {
				for (ShopData data : plugin.getShopConfig().getStickDatas()) {
					if (item.getItemMeta().getDisplayName().equals(data.getShopItem().getItemMeta().getDisplayName())) {

						p.closeInventory();
						p.playSound(p.getLocation(), Sound.CLICK, 10, 10);
						plugin.getShopManager().hasPurchased(p.getUniqueId(),
								plugin.getShopManager().findByShopItem(item, ShopType.STICK).getId(), ShopType.STICK,
								hasPurchased -> {
									if (!hasPurchased) {
										p.sendMessage(plugin.getPrefix() + "§7Du hast den §7"
												+ data.getItem().getItemMeta().getDisplayName()
												+ " §7Stick nicht in besitz.");
										return;
									}

									plugin.getData().getPlayerDatas().get(p.getUniqueId()).setSelectedStick(data);
									plugin.getShopManager().setSelected(p.getUniqueId(), ShopType.STICK, data.getId());
									plugin.getData().getPlayerDatas().get(p.getUniqueId())
											.setStickName(data.getItemName());
									p.sendMessage(plugin.getPrefix() + "§7Dein Standard-Stick ist nun §8'"
											+ data.getItem().getItemMeta().getDisplayName() + "§8'§7.");
									PlayerInventory.giveInventory(p);
								});
					}

				}
			}
			return;
		}

		if (inv.getName().equalsIgnoreCase("§cShop §8» §eBlöcke")) {
			e.setCancelled(true);
			if (item.getItemMeta().getDisplayName().equals("§8» §cZurück")) {
				p.openInventory(plugin.getShopInventory().getMenu(p.getUniqueId()));
				p.playSound(p.getLocation(), Sound.CLICK, 10, 10);
				return;
			}
			if (e.getClick().equals(ClickType.RIGHT)) {
				for (ShopData data : plugin.getShopConfig().getBlockDatas()) {
					if (item.getItemMeta().getDisplayName().equals(data.getShopItem().getItemMeta().getDisplayName())) {

						p.closeInventory();
						p.playSound(p.getLocation(), Sound.CLICK, 10, 10);

						plugin.getShopManager().hasPurchased(p.getUniqueId(),
								plugin.getShopManager().findByShopItem(item, ShopType.BLOCK).getId(), ShopType.BLOCK,
								hasPurchased -> {
									if (hasPurchased) {
										p.sendMessage(plugin.getPrefix() + "§7Du hast den §7"
												+ data.getItem().getItemMeta().getDisplayName()
												+ " §7Block bereits gekauft.");
										return;
									}

									plugin.getCoinsAPI().hasCoins(p.getUniqueId(), data.getPrice(), hasCoins -> {
										if (!hasCoins) {
											plugin.getCoinsAPI().getCoins(p.getUniqueId(), coins -> {
												p.sendMessage(plugin.getPrefix() + "§7Dir fehlen noch §e"
														+ (data.getPrice() - coins) + " §7Coins.");

											});
											return;
										}
										plugin.getCoinsAPI().removeCoins(p.getUniqueId(), data.getPrice());
										plugin.getShopManager().addPurchase(p.getUniqueId(), ShopType.BLOCK,
												plugin.getShopManager().findByShopItem(item, ShopType.BLOCK).getId());
										p.sendMessage(plugin.getPrefix() + "§7Du hast den Block §8'"
												+ data.getItem().getItemMeta().getDisplayName() + "§8' §7gekauft.");
									});

								});

					}
				}
			} else {
				for (ShopData data : plugin.getShopConfig().getBlockDatas()) {
					if (item.getItemMeta().getDisplayName().equals(data.getShopItem().getItemMeta().getDisplayName())) {

						p.closeInventory();
						p.playSound(p.getLocation(), Sound.CLICK, 10, 10);
						plugin.getShopManager().hasPurchased(p.getUniqueId(),
								plugin.getShopManager().findByShopItem(item, ShopType.BLOCK).getId(), ShopType.BLOCK,
								hasPurchased -> {
									if (!hasPurchased) {
										p.sendMessage(plugin.getPrefix() + "§7Du hast den §7"
												+ data.getItem().getItemMeta().getDisplayName()
												+ " §7Block nicht in besitz.");
										return;
									}

									plugin.getData().getPlayerDatas().get(p.getUniqueId()).setSelectedBlock(data);
									plugin.getShopManager().setSelected(p.getUniqueId(), ShopType.BLOCK, data.getId());
									plugin.getData().getPlayerDatas().get(p.getUniqueId())
											.setBlockName(data.getItemName());
									p.sendMessage(plugin.getPrefix() + "§7Dein Standard-Block ist nun §8'"
											+ data.getItem().getItemMeta().getDisplayName() + "§8'§7.");
									PlayerInventory.giveInventory(p);
								});
					}
				}
			}
			return;
		}
	}
}

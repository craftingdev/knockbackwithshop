package eu.MyPvP.knockback.inventorys;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;

import eu.MyPvP.knockback.KnockBack;
import eu.MyPvP.knockback.utils.ItemCreator;

/*
* Created by MarvCode at 22.08.2019 15:54:21
*/

public class InventoryOrder {
	private KnockBack plugin;

	public InventoryOrder(final KnockBack plugin) {
		this.plugin = plugin;
	}

	public Inventory getInventory(final UUID uuid) {
		Inventory inv = Bukkit.createInventory(null, 9, "§cInventar §8» §7Sortierung");
		plugin.getInventorySlots().getArrow(uuid, slot -> {
			inv.setItem(slot, new ItemCreator(Material.ARROW).setAmount(1).build());
		});

		plugin.getInventorySlots().getBow(uuid, slot -> {
			inv.setItem(slot, new ItemCreator(Material.BOW).addEnchantment(Enchantment.DURABILITY, 2, true)
					.addItemFlags(ItemFlag.HIDE_ENCHANTS).setAmount(1).setUnbreakable(true).build());
		});
		plugin.getInventorySlots().getStick(uuid, slot -> {
			inv.setItem(slot,
					plugin.getData().getPlayerDatas().get(uuid).getSelectedStick().getItem());
		});

		plugin.getInventorySlots().getStone(uuid, slot -> {

			inv.setItem(slot,
					plugin.getData().getPlayerDatas().get(uuid).getSelectedBlock().getItem());
		});

		plugin.getInventorySlots().getCobweb(uuid, slot -> {
			inv.setItem(slot, new ItemCreator(Material.WEB).setAmount(1).build());
		});

		plugin.getInventorySlots().getEnderpearl(uuid, slot -> {
			inv.setItem(slot, new ItemCreator(Material.ENDER_PEARL).setAmount(1).build());
		});
		return inv;
	}
}

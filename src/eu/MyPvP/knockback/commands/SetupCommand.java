package eu.MyPvP.knockback.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import eu.MyPvP.knockback.KnockBack;

/*
* Created by MarvCode at 20.08.2019 14:05:28
*/

public class SetupCommand implements CommandExecutor {
	private KnockBack plugin;

	public SetupCommand(final KnockBack plugin) {
		this.plugin = plugin;
	}

	
	@Override
	public boolean onCommand(CommandSender s, Command c, String l, String[] a) {
		if (!(s instanceof Player))
			return true;

		final Player p = (Player) s;
		
		if (!p.hasPermission("knockback.setup")) {
			p.sendMessage(plugin.getNoPermissions());
			return true;
		}
		
		// a0 a1 a2
		if (a.length == 2) {

			if (a[0].equalsIgnoreCase("create")) {
				if (plugin.getLocationManager().getLocations().get(a[1]) != null) {
					p.sendMessage(plugin.getPrefix() + "�7Diese Map existiert bereits.");
					return true;
				}
				plugin.getLocationManager().setSpawn(p.getLocation(), a[1]);
				plugin.getLocationManager().loadLocations();
				p.sendMessage(
						plugin.getPrefix() + "�7Die Map �c" + a[1] + " �7wurde erfolgreich erstellt.");

				return true;
			}

			if (a[0].equalsIgnoreCase("setspawnhigh")) {
				if (plugin.getLocationManager().getLocations().get(a[1]) == null) {
					p.sendMessage(plugin.getPrefix() + "�7Diese Map existiert nicht.");
					return true;
				}

				plugin.getLocationManager().setSpawnHigh(p.getLocation(), a[1]);
				p.sendMessage(plugin.getPrefix() + "�7Die Spawn-H�he von der Map �c" + a[1]
						+ " �7wurde erfolgreich gesetzt.");

				return true;
			}
			
			if (a[0].equalsIgnoreCase("settopkillslife")) {
				if (plugin.getLocationManager().getLocations().get(a[1]) == null) {
					p.sendMessage(plugin.getPrefix() + "�7Diese Map existiert nicht.");
					return true;
				}

				plugin.getLocationManager().setHoloKillsLocation(p.getLocation(), a[1]);
				p.sendMessage(plugin.getPrefix() + "�7Das Top-Kills Hologram von der Map �c" + a[1]
						+ " �7wurde erfolgreich gesetzt.");

				return true;
			}
			
			if (a[0].equalsIgnoreCase("settopkillsday")) {
				if (plugin.getLocationManager().getLocations().get(a[1]) == null) {
					p.sendMessage(plugin.getPrefix() + "�7Diese Map existiert nicht.");
					return true;
				}

				plugin.getLocationManager().setHoloKillsDayLocation(p.getLocation(), a[1]);
				p.sendMessage(plugin.getPrefix() + "�7Das Top-Streaks Hologram von der Map �c" + a[1]
						+ " �7wurde erfolgreich gesetzt.");

				return true;
			}

			if (a[0].equalsIgnoreCase("setdeathhigh")) {
				if (plugin.getLocationManager().getLocations().get(a[1]) == null) {
					p.sendMessage(plugin.getPrefix() + "�7Diese Map existiert nicht.");
					return true;
				}

				plugin.getLocationManager().setDeathHigh(p.getLocation(), a[1]);
				p.sendMessage(plugin.getPrefix() + "�7Die Todes-H�he von der Map �c" + a[1]
						+ " �7wurde erfolgreich gesetzt.");

				return true;
			}
		} else {
			p.sendMessage(plugin.getPrefix());
			p.sendMessage(plugin.getPrefix() + " �8- �7/setup create (�cMap-Name�7)");
			p.sendMessage(plugin.getPrefix() + " �8- �7/setup setspawnhigh (�cMap-Name�7)");
			p.sendMessage(plugin.getPrefix() + " �8- �7/setup setdeathhigh (�cMap-Name�7)");
			p.sendMessage(plugin.getPrefix() + " �8- �7/setup settopkillslife (�cMap-Name�7)");
			p.sendMessage(plugin.getPrefix() + " �8- �7/setup settopkillsday (�cMap-Name�7)");
			p.sendMessage(plugin.getPrefix());

		}
		return false;
	}

}

package eu.MyPvP.knockback.listeners;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.DyeColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import eu.MyPvP.knockback.KnockBack;

/*
* Created by MarvCode at 22.08.2019 15:57:24
*/

public class BlockPlaceListener implements Listener {
	private KnockBack plugin;

	public BlockPlaceListener(final KnockBack plugin) {
		this.plugin = plugin;
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlace(BlockPlaceEvent e) {
		Player p = e.getPlayer();
		Block b = e.getBlock();

		if (!plugin.getData().getBuildMode().isEmpty()) {
			if (plugin.getData().getBuildMode().contains(p)) {
				return;
			}
		}

		if (e.getBlockPlaced().getLocation().getY() > plugin.getLocationManager().getSpawnHighs()
				.get(plugin.getMapChange().getCurrentMap())) {
			e.setCancelled(true);
			return;
		}

		if (e.getBlockPlaced() == null || e.getBlockPlaced().getType().equals(Material.AIR))
			return;

		List<Location> locs = new ArrayList<>();
		List<Material> materials = new ArrayList<>();
		List<Byte> bytes = new ArrayList<>();

		materials.add(b.getType());
		bytes.add(b.getData());
		locs.add(b.getLocation());

		new BukkitRunnable() {

			@Override
			public void run() {
				for (Location loc : locs) {
					for (Material material : materials) {
						for (Byte by : bytes) {

							Block block = loc.getBlock();
							if (block.getType() == Material.WEB)
								return;
							block.setType(Material.STAINED_CLAY);
							block.setData(DyeColor.YELLOW.getData());

							materials.add(b.getType());
							bytes.add(b.getData());
							locs.add(b.getLocation());

							locs.remove(loc);
							bytes.remove(by);
							materials.remove(material);
						}
					}
				}
			}
		}.runTaskLater(plugin, 30);

		new BukkitRunnable() {

			@Override
			public void run() {
				for (Location loc : locs) {
					for (Material material : materials) {
						for (Byte by : bytes) {
							Block block = loc.getBlock();
							if (block.getType() == Material.STAINED_CLAY
									&& block.getData() == DyeColor.YELLOW.getData()) {
								block.setType(Material.STAINED_CLAY);
								block.setData(DyeColor.RED.getData());

								materials.add(b.getType());
								bytes.add(b.getData());
								locs.add(b.getLocation());

								locs.remove(loc);
								bytes.remove(by);
								materials.remove(material);
							}
						}
					}
				}
			}
		}.runTaskLater(plugin, 60);

		new BukkitRunnable() {

			@Override
			public void run() {
				for (Location loc : locs) {
					for (Byte by : bytes) {
						Block block = loc.getBlock();

						if (block.getType() == Material.STAINED_CLAY && block.getData() == DyeColor.RED.getData()) {
							block.setType(Material.AIR);
							block.setData(by);
							block.getWorld().playEffect(block.getLocation(), Effect.SMOKE, 20);

							if (p == null)
								return;

							plugin.getInventorySlots().getStone(p.getUniqueId(), stone -> {

								if (stone == null)
									return;

								if (p.getInventory().getItem(stone) == null) {
									if (p != null) {
										if (plugin.getData().getPlayerDatas().get(p.getUniqueId()) == null)
											return;

										ItemStack is = plugin.getData().getPlayerDatas().get(p.getUniqueId())
												.getSelectedBlock().getItem();
										is.setAmount(1);
										p.getInventory().setItem(stone, is);
										is.setAmount(32);
										return;
									}
								}

								if (p.getInventory().getItem(stone).getAmount() != 32) {
									if (p != null) {
										if (plugin.getData().getPlayerDatas().get(p.getUniqueId()) == null)
											return;

										ItemStack is = plugin.getData().getPlayerDatas().get(p.getUniqueId())
												.getSelectedBlock().getItem();
										is.setAmount(1);
										p.getInventory().addItem(is);
										is.setAmount(32);
										p.playSound(p.getLocation(), Sound.ITEM_PICKUP, 10, 10);
									}
								}

							});

						}else {
							block.setType(Material.AIR);
							block.setData(by);
							block.getWorld().playEffect(block.getLocation(), Effect.SMOKE, 20);
						}

					}

				}

			}
		}.runTaskLater(plugin, 90);
	}
}

package eu.MyPvP.knockback.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import eu.MyPvP.knockback.KnockBack;

/*
* Created by MarvCode at 22.08.2019 18:27:54
*/

public class BlockBreakListener implements Listener {
	private KnockBack plugin;

	public BlockBreakListener(final KnockBack plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onBreak(final BlockBreakEvent e) {
		if (plugin.getData().getBuildMode().contains(e.getPlayer()))
			return;

		e.setCancelled(true);
	}
}

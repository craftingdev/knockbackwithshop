package eu.MyPvP.knockback.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import eu.MyPvP.knockback.KnockBack;

/*
* Created by MarvCode at 22.08.2019 16:29:02
*/

public class InvCommand implements CommandExecutor {
	private KnockBack plugin;

	public InvCommand(final KnockBack plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender s, Command c, String l, String[] a) {
		if (!(s instanceof Player))
			return true;

		final Player p = (Player) s;

		if (p.getLocation().getY() < plugin.getLocationManager().getSpawnHighs()
				.get(plugin.getMapChange().getCurrentMap())) {
			p.sendMessage(plugin.getPrefix() + "�7Du kannst dein �eInventar �7nur am �eSpawn �7sortieren.");
			return true;	
		}

		p.openInventory(plugin.getInvOrder().getInventory(p.getUniqueId()));
		return false;
	}

}
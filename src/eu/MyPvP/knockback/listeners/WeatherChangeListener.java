package eu.MyPvP.knockback.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;

import eu.MyPvP.knockback.KnockBack;

/*
* Created by MarvCode at 27.08.2019 11:45:37
*/

public class WeatherChangeListener implements Listener {
	@SuppressWarnings("unused")
	private KnockBack plugin;

	public WeatherChangeListener(final KnockBack plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onWeatherChange(final WeatherChangeEvent e) {
		e.setCancelled(true);
	}
}

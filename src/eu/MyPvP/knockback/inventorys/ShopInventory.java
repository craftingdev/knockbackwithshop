package eu.MyPvP.knockback.inventorys;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import eu.MyPvP.knockback.KnockBack;
import eu.MyPvP.knockback.enums.ShopType;
import eu.MyPvP.knockback.utils.ItemCreator;
import eu.MyPvP.knockback.utils.ShopData;

/*
* Created by MarvCode at 23.08.2019 11:46:46
*/

public class ShopInventory {

	@SuppressWarnings("deprecation")
	public Inventory getMenu(final UUID uuid) {
		Inventory inv = Bukkit.createInventory(null, 3 * 9, "§cShop §8» §eMenü");

		for (int i = 0; i != inv.getSize(); i++) {
			inv.setItem(i, new ItemCreator(Material.STAINED_GLASS_PANE, 1, DyeColor.GRAY.getData()).setDisplayname("§c")
					.build());
		}

		inv.setItem(10,
				new ItemCreator(Material.STICK).setAmount(1).setDisplayname("§8● §eSticks §8●")
						.setLore(Arrays.asList("", "§8» §7Kaufe oder wähle ein §eStick§7.", "",
								"§7Standard §8» " + KnockBack.getInstance().getData().getPlayerDatas().get(uuid)
										.getSelectedStick().getItem().getItemMeta().getDisplayName()))
						.build());

		inv.setItem(13,
				new ItemCreator(Material.SANDSTONE).setAmount(1).setDisplayname("§8● §eBlöcke §8●")
						.setLore(Arrays.asList("", "§8» §7Kaufe oder wähle eine §eBlockart§7.", "",
								"§7Standard §8» " + KnockBack.getInstance().getData().getPlayerDatas().get(uuid)
										.getSelectedBlock().getItem().getItemMeta().getDisplayName()))
						.build());

		inv.setItem(16, new ItemCreator(Material.CHEST).setAmount(1).setDisplayname("§8● §eInventarsortierung §8●")
				.setLore(Arrays.asList("", "§8» §7Öffne die §eInventar§7-§eSortierung§7.")).build());

		return inv;
	}

	@SuppressWarnings("deprecation")
	public Inventory getSticks(final UUID uuid) {
		Inventory inv = Bukkit.createInventory(null, 4 * 9, "§cShop §8» §eSticks");

		for (int i = 0; i != inv.getSize(); i++) {
			inv.setItem(i, new ItemCreator(Material.STAINED_GLASS_PANE, 1, DyeColor.GRAY.getData()).setDisplayname("§c")
					.build());
		}

		for (ShopData data : KnockBack.getInstance().getShopConfig().getStickDatas()) {
			ItemStack is = data.getShopItem();
			ItemMeta im = is.getItemMeta();
			List<String> lore = im.getLore();
			is.setItemMeta(im);

			inv.setItem(data.getSlot(),
					new ItemCreator(data.getItem().getType()).setAmount(1)
							.setDisplayname(data.getItem().getItemMeta().getDisplayName())
							.setLore(Arrays.asList("", "§eLinksklick §8» §7Auswählen", "§eRechtsklick §8» §7Kaufen", "",
									"§7Preis §8» §e" + data.getPrice(), "", "§7Status §8» §7" + "§a§l✔"))
							.build());

			KnockBack.getInstance().getShopManager().hasPurchased(uuid, data.getId(), ShopType.STICK, hasPurchased -> {
				if (hasPurchased) {
					inv.setItem(data.getSlot(),
							new ItemCreator(data.getItem().getType()).setAmount(1)
									.setDisplayname(data.getItem().getItemMeta().getDisplayName())
									.addEnchantment(Enchantment.ARROW_DAMAGE, 1, true).addItemFlags(ItemFlag.HIDE_ENCHANTS)
									.setLore(Arrays.asList("", "§eLinksklick §8» §7Auswählen",
											"§eRechtsklick §8» §7Kaufen", "", "§7Preis §8» §e" + data.getPrice(), "",
											"§7Status §8» §7" + "§a§l✔"))
									.build());
				} else if (!hasPurchased) {
					inv.setItem(data.getSlot(), new ItemCreator(data.getItem().getType()).setAmount(1)
							.setDisplayname(data.getItem().getItemMeta().getDisplayName())
							.setLore(Arrays.asList("", "§eLinksklick §8» §7Auswählen", "§eRechtsklick §8» §7Kaufen", "",
									"§7Preis §8» §e" + data.getPrice(), "", "§7Status §8» §7" + "§c§l✘"))
							.build());
				}
			});

			im.setLore(lore);
		}

		inv.setItem(35, new ItemCreator(Material.BARRIER).setDisplayname("§8» §cZurück")
				.setLore(Arrays.asList("", "§8» §7Kehre zurück zum §eMenü§7.")).setAmount(1).build());

		return inv;
	}

	@SuppressWarnings("deprecation")
	public Inventory getBlocks(final UUID uuid) {
		Inventory inv = Bukkit.createInventory(null, 4 * 9, "§cShop §8» §eBlöcke");

		for (int i = 0; i != inv.getSize(); i++) {
			inv.setItem(i, new ItemCreator(Material.STAINED_GLASS_PANE, 1, DyeColor.GRAY.getData()).setDisplayname("§c")
					.build());
		}

		for (ShopData data : KnockBack.getInstance().getShopConfig().getBlockDatas()) {
			ItemStack is = data.getShopItem();
			ItemMeta im = is.getItemMeta();
			List<String> lore = im.getLore();

			im.setLore(lore);
			is.setItemMeta(im);

			KnockBack.getInstance().getShopManager().hasPurchased(uuid, data.getId(), ShopType.BLOCK, hasPurchased -> {

				if (hasPurchased) {
					inv.setItem(data.getSlot(),
							new ItemCreator(data.getItem().getType()).setAmount(1)
									.setDisplayname(data.getItem().getItemMeta().getDisplayName())
									.addEnchantment(Enchantment.ARROW_DAMAGE, 1, true).addItemFlags(ItemFlag.HIDE_ENCHANTS)
									.setLore(Arrays.asList("", "§eLinksklick §8» §7Auswählen",
											"§eRechtsklick §8» §7Kaufen", "", "§7Preis §8» §e" + data.getPrice(), "",
											"§7Status §8» §7" + "§a§l✔"))
									.build());
				} else if (!hasPurchased) {
					inv.setItem(data.getSlot(), new ItemCreator(data.getItem().getType()).setAmount(1)
							.setDisplayname(data.getItem().getItemMeta().getDisplayName())
							.setLore(Arrays.asList("", "§eLinksklick §8» §7Auswählen", "§eRechtsklick §8» §7Kaufen", "",
									"§7Preis §8» §e" + data.getPrice(), "", "§7Status §8» §7" + "§c§l✘"))
							.build());
				}
			});
		}

		inv.setItem(35, new ItemCreator(Material.BARRIER).setDisplayname("§8» §cZurück")
				.setLore(Arrays.asList("", "§8» §7Kehre zurück zum §eMenü§7.")).setAmount(1).build());

		return inv;
	}
}

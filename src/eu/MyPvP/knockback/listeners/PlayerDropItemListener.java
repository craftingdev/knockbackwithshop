package eu.MyPvP.knockback.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

import eu.MyPvP.knockback.KnockBack;

/*
* Created by MarvCode at 25.08.2019 18:07:34
*/

public class PlayerDropItemListener implements Listener {
	private KnockBack plugin;

	public PlayerDropItemListener(final KnockBack plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onDrop(final PlayerDropItemEvent e) {
		if (!plugin.getData().getBuildMode().contains(e.getPlayer())) {
			e.setCancelled(true);
		}
	}
}

package eu.MyPvP.knockback.enums;

/*
* Created by MarvCode at 16.09.2019 13:55:55
*/

public enum ShopType {
	BLOCK ("BLOCK"),
	STICK ("STICK");
	
	String typeName;
	
	private ShopType(final String typeName) {
		this.typeName = typeName;
	}
	
	public String getTypeName() {
		return typeName;
	}
}
